﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(PlataformaCursosOnline.Startup))]
namespace PlataformaCursosOnline
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
