using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace trabajo_lp2
{
    public class Usuario
    {
        private int id;
        private String usuario;
        private String contraseña;
        private String nombre;

        protected Usuario(int id, String usuario, String nombre, String contraseña)
        {
            this.usuario = usuario;
            this.contraseña = contraseña;
            this.nombre = nombre;
            this.id = id;
        }
        protected Usuario ()
        {

        }

        protected Usuario(String usuario, String contraseña)
        {
            this.usuario = usuario;
            this.contraseña = contraseña;
        }

        public int getId()
        {
            return id;
        }

        public void setId(int id)
        {
            this.id = id;
        }

        public String getNombre()
        {
            return nombre;
        }

        public void setNombre(String nombre)
        {
            this.nombre = nombre;
        }

        public String getContraseña()
        {
            return contraseña;
        }

        public void setContraseña(String contraseña)
        {
            this.contraseña = contraseña;
        }


        public String getUsuario()
        {
            return usuario;
        }

        public void setUsuario(String usuario)
        {
            this.usuario = usuario;
        }
    }
}
