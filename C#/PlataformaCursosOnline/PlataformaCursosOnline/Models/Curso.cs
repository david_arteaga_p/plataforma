using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace trabajo_lp2
{
    public class Curso
    {
        private int id;
        private String nombre;
        private double costo;
        private int horasDuracion;
        private List<Profesor> profesores;
        private Empresa empresa;
        private DisponibilidadSemanal asesoriasSemanales;
        private List<Alumno> alumnos;
        private bool disponible;
        private DateTime fechaInicio;
        private DateTime fechaFin;

        public Curso(int id, String nombre, double costo, int horasDuracion, List<Profesor> profesores, Empresa empresa, DisponibilidadSemanal asesoriasSemanales, List<Alumno> alumnos)
        {
            this.empresa = empresa;
            this.asesoriasSemanales = asesoriasSemanales;
            this.alumnos = alumnos;
            this.setId(id);
            this.setNombre(nombre);
            this.setCosto(costo);
            this.setHorasDuracion(horasDuracion);
            this.setProfesores(profesores);
        }

        public Curso(){

        }


        public int getId()
        {
            return id;
        }

        public void setId(int id)
        {
            this.id = id;
        }

        public String getNombre()
        {
            return nombre;
        }

        public void setNombre(String nombre)
        {
            this.nombre = nombre;
        }

        public double getCosto()
        {
            return costo;
        }

        public void setCosto(double costo)
        {
            this.costo = costo;
        }

        public int getHorasDuracion()
        {
            return horasDuracion;
        }

        public void setHorasDuracion(int horasDuracion)
        {
            this.horasDuracion = horasDuracion;
        }

        public List<Profesor> getProfesores()
        {
            return profesores;
        }

        public void setProfesores(List<Profesor> profesores)
        {
            this.profesores = profesores;
        }

        public Empresa getEmpresa()
        {
            return empresa;
        }

        public void setEmpresa(Empresa empresa)
        {
            this.empresa = empresa;
        }

        public DisponibilidadSemanal getAsesoriasSemanales()
        {
            return asesoriasSemanales;
        }

        public void setAsesoriasSemanales(DisponibilidadSemanal asesoriasSemanales)
        {
            this.asesoriasSemanales = asesoriasSemanales;
        }

        public List<Alumno> getAlumnos()
        {
            return alumnos;
        }

        public void setAlumnos(List<Alumno> alumnos)
        {
            this.alumnos = alumnos;
        }
    }
}