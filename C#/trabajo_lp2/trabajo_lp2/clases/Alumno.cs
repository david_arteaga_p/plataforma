using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace trabajo_lp2
{
    public class Alumno : Usuario
    {

        public void registrarPago(Pago pago)
        {

        }

        private String email;
        private String contraseña;
        private List<Curso> cursos;

        public Alumno(int id, String usuario, String nombre, String apellido, String email, String contraseña, List<Curso> cursos) :
            base(id, usuario, nombre, apellido)
        {

            this.email = email;
            this.contraseña = contraseña;
            this.cursos = cursos;
        }

        public String getEmail()
        {
            return email;
        }

        public void setEmail(String email)
        {
            this.email = email;
        }

        public String getContraseña()
        {
            return contraseña;
        }

        public void setContraseña(String contraseña)
        {
            this.contraseña = contraseña;
        }

        public List<Curso> getCursos()
        {
            return cursos;
        }

        public void setCursos(List<Curso> cursos)
        {
            this.cursos = cursos;
        }
    }
}