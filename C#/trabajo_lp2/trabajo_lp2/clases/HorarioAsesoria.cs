using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace trabajo_lp2
{
    public class HorarioAsesoria
    {
        private DateTime horaInicial;
        private DateTime horaFinal;
        private Profesor profesor;

        public HorarioAsesoria(DateTime horaInicial, DateTime horaFinal, Profesor profesor)
        {
            this.horaInicial = horaInicial;
            this.horaFinal = horaFinal;
            this.profesor = profesor;
        }

        public DateTime getHoraInicial()
        {
            return horaInicial;
        }

        public void setHoraInicial(DateTime horaInicial)
        {
            this.horaInicial = horaInicial;
        }

        public DateTime getHoraFinal()
        {
            return horaFinal;
        }

        public void setHoraFinal(DateTime horaFinal)
        {
            this.horaFinal = horaFinal;
        }

        public Profesor getProfesor()
        {
            return profesor;
        }

        public void setProfesor(Profesor profesor)
        {
            this.profesor = profesor;
        }
    }
}