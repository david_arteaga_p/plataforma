using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace trabajo_lp2
{
    public class Mensaje
    {
        String contenido;
        Usuario remitente;
        Usuario destinatario;

        public Mensaje(String contenido, Usuario remitente, Usuario destinatario)
        {
            this.contenido = contenido;
            this.remitente = remitente;
            this.destinatario = destinatario;
        }

        public String getContenido()
        {
            return contenido;
        }

        public void setContenido(String contenido)
        {
            this.contenido = contenido;
        }

        public Usuario getRemitente()
        {
            return remitente;
        }

        public void setRemitente(Usuario remitente)
        {
            this.remitente = remitente;
        }

        public Usuario getDestinatario()
        {
            return destinatario;
        }

        public void setDestinatario(Usuario destinatario)
        {
            this.destinatario = destinatario;
        }
    }
}