using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace trabajo_lp2
{
    public class Usuario
    {
        private int id;
        private String usuario;
        private String nombre;
        private String apellido;

        protected Usuario(int id, String usuario, String nombre, String apellido)
        {
            this.usuario = usuario;
            this.apellido = apellido;
            this.nombre = nombre;
            this.id = id;
        }

        public int getId()
        {
            return id;
        }

        public void setId(int id)
        {
            this.id = id;
        }

        public String getNombre()
        {
            return nombre;
        }

        public void setNombre(String nombre)
        {
            this.nombre = nombre;
        }

        public String getApellido()
        {
            return apellido;
        }

        public void setApellido(String apellido)
        {
            this.apellido = apellido;
        }

        public String getUsuario()
        {
            return usuario;
        }

        public void setUsuario(String usuario)
        {
            this.usuario = usuario;
        }
    }
}
