using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
using System.Threading.Tasks;

namespace trabajo_lp2
{
    public class Pago
    {
        private int id;
        private double monto;
        private DateTime fecha;
        private RegionInfo tipoDeMoneda;
        private Curso curso;
        private Alumno alumno;

        public Pago(int id, double monto, DateTime fecha, RegionInfo tipoDeMoneda, Curso curso, Alumno alumno)
        {
            this.id = id;
            this.monto = monto;
            this.fecha = fecha;
            this.tipoDeMoneda = tipoDeMoneda;
            this.curso = curso;
            this.alumno = alumno;
        }

        public int getId()
        {
            return id;
        }

        public void setId(int id)
        {
            this.id = id;
        }

        public double getMonto()
        {
            return monto;
        }

        public void setMonto(double monto)
        {
            this.monto = monto;
        }

        public DateTime getFecha()
        {
            return fecha;
        }

        public void setFecha(DateTime fecha)
        {
            this.fecha = fecha;
        }

        public RegionInfo getTipoDeMoneda()
        {
            return tipoDeMoneda;
        }

        public void setTipoDeMoneda(RegionInfo tipoDeMoneda)
        {
            this.tipoDeMoneda = tipoDeMoneda;
        }

        public Curso getCurso()
        {
            return curso;
        }

        public void setCurso(Curso curso)
        {
            this.curso = curso;
        }

        public Alumno getAlumno()
        {
            return alumno;
        }

        public void setAlumno(Alumno alumno)
        {
            this.alumno = alumno;
        }
    }
}