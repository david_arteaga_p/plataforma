using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace trabajo_lp2
{
    public class Alumno : Usuario
    {


        private String email;
        private List<Curso> cursos;

        public Alumno(int id, String usuario, String nombre, String apellido, String email, List<Curso> cursos) :
            base(id, usuario, nombre, apellido)
        {

            this.email = email;
            this.cursos = cursos;
        }

        public Alumno(int id, String usuario, String nombre, String apellido, String email) :
            base(id, usuario, nombre, apellido)
        {

            this.email = email;
        }


        public Alumno()
        {

        }

        public String getEmail()
        {
            return email;
        }

        public void setEmail(String email)
        {
            this.email = email;
        }
        
        public List<Curso> getCursos()
        {
            return cursos;
        }

        public void setCursos(List<Curso> cursos)
        {
            this.cursos = cursos;
        }
    }
}