using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace trabajo_lp2
{
    public class Pregunta
    {
        int id;
        String titulo;
        Alumno alumno;
        Profesor profesor;
        Curso curso;
        List<Mensaje> mensajes;

        public Pregunta(int id, String titulo, Alumno alumno, Profesor profesor, Curso curso, List<Mensaje> mensajes)
        {
            this.id = id;
            this.titulo = titulo;
            this.alumno = alumno;
            this.profesor = profesor;
            this.curso = curso;
            this.mensajes = mensajes;
        }

        public Pregunta()
        {

        }

        public int getId()
        {
            return id;
        }

        public void setId(int id)
        {
            this.id = id;
        }

        public String getTitulo()
        {
            return titulo;
        }

        public void setTitulo(String titulo)
        {
            this.titulo = titulo;
        }

        public Alumno getAlumno()
        {
            return alumno;
        }

        public void setAlumno(Alumno alumno)
        {
            this.alumno = alumno;
        }

        public Profesor getProfesor()
        {
            return profesor;
        }

        public void setProfesor(Profesor profesor)
        {
            this.profesor = profesor;
        }

        public Curso getCurso()
        {
            return curso;
        }

        public void setCurso(Curso curso)
        {
            this.curso = curso;
        }

        public List<Mensaje> getMensajes()
        {
            return mensajes;
        }

        public void setMensajes(List<Mensaje> mensajes)
        {
            this.mensajes = mensajes;
        }
    }
}
