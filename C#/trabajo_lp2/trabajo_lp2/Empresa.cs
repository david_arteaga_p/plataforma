using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace trabajo_lp2
{

    public class Empresa: Usuario
    {
        private int id;
        private String nombre;
        private String rubro;
        private List<Curso> cursos;
        private List<Profesor> profesores;
        
        //public Empresa(int id, String nombre, String rubro, List<Curso> cursos, List<Profesor> profesores, String usuario, String contraseņa): base(usuario, contraseņa)
        //{
        //    this.id = id;
        //    this.nombre = nombre;
        //    this.rubro = rubro;
        //    this.cursos = cursos;
        //    this.profesores = profesores;
        //}No se usa en la vida real, planteamiento de pantallas no lo va permitir por temas de usabilidad.


        public Empresa(int id, String nombre, String rubro,String usuario, String contraseņa): base(usuario, contraseņa)
        {
            this.id = id;
            this.nombre = nombre;
            this.rubro = rubro;
        }

        public Empresa()
        {

        }

        //public void AgregarProfesor(Profesor profe){
        //    profesores.Add(profe);
        //}
        
        //public void AgregarCurso(Curso cur)
        //{
        //   cursos.Add(cur);
        //}

        public int getId()
        {
            return id;
        }

        public void setId(int id)
        {
            this.id = id;
        }

        public String getNombre()
        {
            return nombre;
        }

        public void setNombre(String nombre)
        {
            this.nombre = nombre;
        }

        public String getRubro()
        {
            return rubro;
        }

        public void setRubro(String rubro)
        {
            this.rubro = rubro;
        }

        public List<Curso> getCursos()
        {
            return cursos;
        }

        public void setCursos(List<Curso> cursos)
        {
            this.cursos = cursos;
        }

        public List<Profesor> getProfesores()
        {
            return profesores;
        }

        public void setProfesores(List<Profesor> profesores)
        {
            this.profesores = profesores;
        }
       
    }
}