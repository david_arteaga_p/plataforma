using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace trabajo_lp2
{
    public class DisponibilidadSemanal
    {
        List<HorarioAsesoria> horariosAsesorias;

        public DisponibilidadSemanal(List<HorarioAsesoria> horariosAsesorias)
        {
            this.horariosAsesorias = horariosAsesorias;
        }
        public DisponibilidadSemanal()
        {

        }
        public List<HorarioAsesoria> getHorariosAsesorias()
        {
            return horariosAsesorias;
        }

        public void setHorariosAsesorias(List<HorarioAsesoria> horariosAsesorias)
        {
            this.horariosAsesorias = horariosAsesorias;
        }
    }
}