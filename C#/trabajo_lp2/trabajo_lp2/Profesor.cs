using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace trabajo_lp2
{
public class Profesor: Usuario {
    private Empresa empresa;
    private List<Curso> cursos;
    private DisponibilidadSemanal disponibilidadSemanal;

    public Profesor(int id, String usuario, String nombre, String apellido, Empresa empresa, List<Curso> cursos, DisponibilidadSemanal disponibilidadSemanal):
    base(id, usuario, nombre, apellido) {
        
        this.empresa = empresa;
        this.cursos = cursos;
        this.disponibilidadSemanal = disponibilidadSemanal;
    }
    public Profesor()
    {

    }
    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public List<Curso> getCursos() {
        return cursos;
    }

    public void setCursos(List<Curso> cursos) {
        this.cursos = cursos;
    }

    public DisponibilidadSemanal getDisponibilidadSemanal() {
        return disponibilidadSemanal;
    }

    public void setDisponibilidadSemanal(DisponibilidadSemanal disponibilidadSemanal) {
        this.disponibilidadSemanal = disponibilidadSemanal;
    }

}
}