$(document).ready(function() {
    // add csrf token to ajax requests
    var token = $("meta[name='_csrf']").attr("content");
    var header = $("meta[name='_csrf_header']").attr("content");
    $(document).ajaxSend(function(e,xhr,options) {
       xhr.setRequestHeader(header, token);
    });


    $("#addProfesorForm").on("submit", function(e) {
        e.preventDefault();
        submitForm(e.target,
            function(response) {
                var newRowProfesor = getRowForProfesor(response.idProfesor, response.nombreProfesor, response.idCurso);
                $("#profesoresElegidosTableBody").append(newRowProfesor);
            },
            function(error) {
                showAlert("#errorAlertContainer", error.responseText);
            }
        );
    });

    // not added on the .deleteProfesorButton directly because it will not work for newly loaded elements
    $("#profesoresElegidosTableBody").on("click", ".deleteProfesorButton", function(e) {
       var button = $(e.currentTarget);
       var idProfesor = button.data("id-profesor");
       var idCurso = button.data("id-curso");

       $.ajax({
           type: "POST",
           url: "/curso/" + idCurso + "/profesores/eliminar",
           data: {'idProfesor': idProfesor},
           success: function(data) {
                $("#profesorRow_" + idProfesor).remove();

                var profesor = profesoresDisponibles[idProfesor];
                if (profesorElegido != null && profesor.idProfesor == profesorElegido.idProfesor) {
                    setProfesorElegido(null);
                }
                profesor.quitarTodosHorariosAsignados();
           },
           error: function(error) {
               showAlert("#errorAlertContainer", error.responseText);
           }
       });
    });

    function submitForm(form, onSuccess, onError, onComplete) {
        form = $(form);
        $.ajax({
            type: form[0].getAttribute("method"),
            url: form[0].getAttribute("action"),
            data: form.serialize(),
            success: onSuccess,
            error: onError,
            complete: onComplete
        });
    };

    function showAlert(alertContainer, message) {
        var text = document.createElement("span");
        text.appendChild(document.createTextNode(message));

        var button = document.createElement("button");
        button.setAttribute("class", "close");
        button.setAttribute("type", "button");
        button.setAttribute("data-dismiss", "alert");

        var span = document.createElement("span");
        span.innerHTML = "&times;";
        button.appendChild(span);

        var alert = document.createElement("div");
        alert.setAttribute("class", "alert alert-danger fade in");
        alert.setAttribute("role", "alert");

        alert.appendChild(text);
        alert.appendChild(button);

        $(alertContainer).append(alert);
    };

    function getRowForProfesor(idProfesor, nombreProfesor, idCurso) {
        var tr = document.createElement("tr");
        tr.setAttribute("id", "profesorRow_" + idProfesor);

        var td1 = document.createElement("td");
        td1.appendChild(document.createTextNode(nombreProfesor));
        td1.setAttribute("class", "nombreProfesor");
        td1.setAttribute("data-id-profesor", idProfesor);
        td1.setAttribute("id", "nombreProfesor_" + idProfesor);
        tr.appendChild(td1);

        var td2 = document.createElement("td");
        tr.appendChild(td2);

        var button = document.createElement("button");
        button.setAttribute("class", "close deleteProfesorButton");
        button.setAttribute("type", "button");
        button.setAttribute("data-id-curso", idCurso);
        button.setAttribute("data-id-profesor", idProfesor);
        td2.appendChild(button);

        var span = document.createElement("span");
        span.setAttribute("aria-hidden", "true");
        span.innerHTML = "&times;";
        button.appendChild(span);

        return tr;
    }


    var regex = new RegExp("(.*/curso/)([0-9]+)/.*");
    var idCurso = regex.exec(document.location.href)[2];

    // mapa de id profesor a profesor; están todos los profesores disponibles de la empresa
    var profesoresDisponibles;

    // profesor cuyos horarios disponibles se están mostrando actualmente
    var profesorElegido;

	var EstadoSlot = {
		VACIO: "vacío",
		CHECKED: "checked",
		MOSTRANDO: "mostrando",
		CHECKED_AFTER_MOSTRAR: "checked_after_mostrar"
	};

	var checkedCssClass = "slot_checked";
	var mostrandoCssClass = "mostrandoProfesorDisponible";


	class Slot {
		constructor(dia, hora) {
			this.dia = dia;
			this.hora = hora;

			this.id = "slot_" + dia + "_" + hora;
			this.td = $("#" + this.id);

			this.estado = EstadoSlot.VACIO;

			// si se ha asignado un profesor a este slot, su información estará en profesorElegido
			this.profesorElegido = null;

			// si se está mostrando que un profesor está disponible en este slot, la información de ese profesor estará en profesorDisponible
			this.profesorDisponible = null;

			this.usedCheck = false
		}

		click() {
			if (this.estado == EstadoSlot.MOSTRANDO) {
				this.check();
			} else if (this.estado == EstadoSlot.CHECKED || this.estado == EstadoSlot.CHECKED_AFTER_MOSTRAR) {
				this.uncheck();
			} else if (this.estado == EstadoSlot.VACIO) {
				console.log("No hay profesor disponible");
			} else {
				throw "ESTADO INVÁLIDO: " + this.id + " " + this.estado;
			}
		}

		checked() {
			return this.estado == EstadoSlot.CHECKED || this.estado == EstadoSlot.CHECKED_AFTER_MOSTRAR;
		}

		checkForProfesor(profesor) {
			// para marcar como elegido un profesor cuando recién se carga la página
			if (this.usedCheck) throw "Solo se usa al inicio para cargar profesor asignado";
			this.usedCheck = true;

			this.estado = EstadoSlot.CHECKED;

			this.profesorElegido = profesor;

			this.td.text(profesor.nombre);
			this.td.addClass(checkedCssClass);
		}

		check() {
			if (this.estado == EstadoSlot.MOSTRANDO) {
				this.estado = EstadoSlot.CHECKED_AFTER_MOSTRAR;

				this.profesorElegido = this.profesorDisponible;

				this.profesorElegido.agregarHorarioAsignado(this.id);

				// nombre queda igual
				this.td.removeClass(mostrandoCssClass);
				this.td.addClass(checkedCssClass);

			} else if (this.estado == EstadoSlot.CHECKED || this.estado == EstadoSlot.CHECKED_AFTER_MOSTRAR) {
				throw "El slot " + this.id + " ya está seleccionado. No puede seleccionarlo";
			} else if (this.estado == EstadoSlot.VACIO) {
				throw new Exception("No hay ningún profesor disponible para este slot " + this.id);
			} else {
				throw "ESTADO INVÁLIDO: " + this.id + " " + this.estado;
			}
		}

		uncheck() {
			if (this.estado == EstadoSlot.CHECKED) {
				this.estado = EstadoSlot.VACIO;

				this.profesorElegido.quitarHorarioAsignado(this.id);
				this.profesorElegido = null;

				this.td.text("");
				this.td.removeClass(checkedCssClass);

			} else if (this.estado == EstadoSlot.CHECKED_AFTER_MOSTRAR) {
				this.estado = EstadoSlot.MOSTRANDO;

				this.profesorElegido.quitarHorarioAsignado(this.id);
				this.profesorElegido = null;

				this.td.text(this.profesorDisponible.nombre);
				this.td.removeClass(checkedCssClass);
				this.td.addClass(mostrandoCssClass);

			} else if (this.estado == EstadoSlot.VACIO || this.estado == EstadoSlot.MOSTRANDO) {
				throw "El slot " + this.id + " no está seleccionado. No puede deseleccionarlo";
			} else {
				throw "ESTADO INVÁLIDO: " + this.id + " " + this.estado;
			}
		}

		mostrarProfesorDisponible(profesor) {
			if (this.estado == EstadoSlot.VACIO) {
				this.estado = EstadoSlot.MOSTRANDO;

				this.profesorDisponible = profesor;

				this.td.text(profesor.nombre);
				this.td.addClass(mostrandoCssClass);

			} else if (this.estado == EstadoSlot.CHECKED) {
				this.estado = EstadoSlot.CHECKED_AFTER_MOSTRAR;

				this.profesorDisponible = profesor;



			} else if (this.estado == EstadoSlot.MOSTRANDO || this.estado == EstadoSlot.CHECKED_AFTER_MOSTRAR) {
				throw "El slot " + this.id + " no puede mostrar un profesor disponible";
			} else {
				throw "ESTADO INVÁLIDO: " + this.id + " " + this.estado;
			}
		}

		ocultarProfesorDisponible() {
			if (this.estado == EstadoSlot.MOSTRANDO) {
				this.estado = EstadoSlot.VACIO;

				this.profesorDisponible = null;

				this.td.text("");
				this.td.removeClass(mostrandoCssClass);

			} else if (this.estado == EstadoSlot.CHECKED_AFTER_MOSTRAR) {
				this.estado = EstadoSlot.CHECKED;

				this.profesorDisponible = null;

				this.td.removeClass(mostrandoCssClass);
				this.td.addClass(checkedCssClass);

			} else if (this.estado == EstadoSlot.VACIO || this.estado == EstadoSlot.CHECKED) {
				throw "El slot " + this.id + " no está mostrando un profesor disponible. No puede dejar de mostrarlo";
			} else {
				throw "ESTADO INVÁLIDO: " + this.id + " " + this.estado;
			}
		}
	};

	class Profesor {
		// los horarios disponibles para el curso incluyen los horarios libres del profesor así como los horarios
		// que ya han sido ocupados con este curso en particular
		constructor(idProfesor, nombre, horariosDisponiblesParaCurso, horariosAsignadosAlCurso) {
			this.idProfesor = idProfesor;
			this.nombre = nombre;
			this.horariosDisponiblesParaCurso = horariosDisponiblesParaCurso;
			this.horariosAsignadosAlCurso = horariosAsignadosAlCurso;

			this.cargado = false;

			this.quitandoTodos = false;
		}

		mostrarHorariosDisponibles() {
			// para cuando se elige un profesor para asignarle horarios
			for (var idSlot of this.horariosDisponiblesParaCurso) {
				var slot = slots[idSlot];
				slot.mostrarProfesorDisponible(this);
			}
		}

		ocultarHorariosDisponibles() {
			// para cuando se deselecciona un profesor o se selecciona otro profesor
			for (var idSlot of this.horariosDisponiblesParaCurso) {
				var slot = slots[idSlot];
				slot.ocultarProfesorDisponible();
			}
		}

		checkHorariosAsignados() {
			// solo para cuando se cargan los datos inicialmente
			if (this.cargado) throw "Can't use twice";
			this.cargado = true;

			for (var idSlot of this.horariosAsignadosAlCurso) {
				var slot = slots[idSlot];
				slot.checkForProfesor(this);
			}
		}

		agregarHorarioAsignado(idSlot) {
			// para cuando se hace click a un slot que está mostrando este profesor
			$.ajax({
			    url: "/curso/" + idCurso + "/agregarSlotProfesor",
			    method: "POST",
			    data: {
			        idProfesor: this.idProfesor,
			        idSlot: idSlot
			    }
			})
			.done(function(response) {
			    console.log(response);
            })
            .fail(function(response) {
                console.log(response);
            });

			this.horariosAsignadosAlCurso.push(idSlot);
		}

		quitarHorarioAsignado(idSlot) {
		    if (!this.quitandoTodos) {
                $.ajax({
                    url: "/curso/" + idCurso + "/eliminarSlotProfesor",
                    method: "POST",
                    data: {
                        idProfesor: this.idProfesor,
                        idSlot: idSlot
                    }
                })
                .done(function(response) {
                    console.log(response);
                })
                .fail(function(response) {
                    console.log(response);
                });
            }

			var index = this.horariosAsignadosAlCurso.indexOf(idSlot);
			if (index == -1) {
				throw "El profesor " + this.id + " no tiene asignado el horario " + idSlot;
			}
			this.horariosAsignadosAlCurso.splice(index, 1);
		}

		quitarTodosHorariosAsignados() {
		    this.quitandoTodos = true;
		    var horariosAsignados = this.horariosAsignadosAlCurso.slice(0);
		    for (var idSlot of horariosAsignados) {
		        var slot = slots[idSlot];
		        slot.uncheck();
		    }
		    this.quitandoTodos = false;
		}
	}

	function getSlots() {
		var slots = {};
		for (var h = 0; h <= 23; h++) {
			for (var d = 1; d <= 7; d++) {
				slots["slot_" + d + "_" + h] = new Slot(d, h);
			}
		}
		return slots;
	}

	function getProfesoresDisponibles() {


	    $.ajax({
	        url: "/curso/" + idCurso + "/profesoresDisponibles"
	    })
	    .done(function(profesoresDisponiblesResponse) {
	        // construir mapa de idProfesor -> profesor de los profesores de la empresa
            profesoresDisponibles = profesoresDisponiblesResponse.reduce(function(map, p) {
                map[p.idProfesor] = new Profesor(p.idProfesor, p.nombre, p.horariosDisponiblesParaCurso, p.horariosAsignadosAlCurso);
                return map;
            }, {});

            // mostrar los horarios ya asignados al curso
            for (profesor of Object.values(profesoresDisponibles)) {
                profesor.checkHorariosAsignados();
            }
	    })
	    .fail(function(response) {
	       console.log(response);
	    });
	}

	function clickSlot(e) {
		var td = $(e.target);
		var idSlot = td.attr("id");
		var slot = slots[idSlot];

		slot.click();
	};

	function setProfesorElegido(profesor) {
	    if (profesorElegido != null) {
	        profesorElegido.ocultarHorariosDisponibles();
	    }
	    profesorElegido = profesor;

	    if (profesorElegido != null) {
            profesorElegido.mostrarHorariosDisponibles();
	    }

	}

	function clickNombreProfesor(e) {
	    var td = $(e.target);
	    var idProfesor = td.attr("data-id-profesor");
	    var profesor = profesoresDisponibles[idProfesor];

	    if (profesorElegido != null && profesor.idProfesor == profesorElegido.idProfesor) {
	        // ya está elegido este profesor
	        profesor = null;
	    }

	    setProfesorElegido(profesor);
	}

	$(".slot").on("click", clickSlot);

	//$(".nombreProfesor").on("click", clickNombreProfesor);
	$("#profesoresElegidosTableBody").on("click", ".nombreProfesor", clickNombreProfesor);

	var slots = getSlots();

	getProfesoresDisponibles();

});