package com.lp2.config.security.password;

import com.lp2.plataforma.empresa.Empresa;

import java.time.LocalDateTime;

/**
 * Created by david on 6/26/17.
 */
public class PasswordResetToken {
    private final Empresa empresa;
    private final String token;
    private final LocalDateTime dateCreated;
    private final boolean active;

    public PasswordResetToken(Empresa empresa, String token, LocalDateTime dateCreated, boolean active) {
        this.empresa = empresa;
        this.token = token;
        this.dateCreated = dateCreated;
        this.active = active;
    }

    @Override
    public String toString() {
        return "PasswordResetToken{" +
                "empresa=" + empresa +
                ", token='" + token + '\'' +
                ", dateCreated=" + dateCreated +
                ", active=" + active +
                '}';
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public String getToken() {
        return token;
    }

    public LocalDateTime getDateCreated() {
        return dateCreated;
    }

    public boolean isActive() {
        return active;
    }
}
