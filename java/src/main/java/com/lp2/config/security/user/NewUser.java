package com.lp2.config.security.user;

/**
 * User that has not been created yet.
 * Created by david on 3/2/17.
 */
public interface NewUser {
    String getUsuario();
    String getPassword();
    boolean isEnabled();
}
