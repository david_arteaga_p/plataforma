package com.lp2.config.security.user.role;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by david on 3/9/17.
 */
@Component
public class RoleMapper implements RowMapper<Role> {
    /**
     * Implementations must implement this getKey to map each row of data
     * in the ResultSet. This getKey should not call {@code next()} on
     * the ResultSet; it is only supposed to map values of the current row.
     *
     * @param rs     the ResultSet to map (pre-initialized for the current row)
     * @param rowNum the number of the current row
     * @return the result object for the current row
     * @throws SQLException if a SQLException is encountered getting
     *                      column values (that is, there's no need to catch SQLException)
     */
    @Override
    public Role mapRow(ResultSet rs, int rowNum) throws SQLException {
        return new Role(
            rs.getInt("idRole"),
            rs.getString("roleName")
        );
    }
}
