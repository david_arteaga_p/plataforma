package com.lp2.config.security.user;

import com.lp2.config.security.user.role.Role;
import com.lp2.config.security.user.role.RoleService;
import com.lp2.plataforma.empresa.Empresa;
import com.lp2.plataforma.empresa.EmpresaService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;

/**
 * Created by david on 3/9/17.
 */
@Component
public class UserMapper implements RowMapper<UserBean> {

    private static final Logger logger = LoggerFactory.getLogger(UserMapper.class);

    private final RoleService roleService;
    private final EmpresaService empresaService;

    @Autowired
    public UserMapper(RoleService roleService, @Lazy EmpresaService empresaService) {
        this.roleService = roleService;
        this.empresaService = empresaService;
    }

    /**
     * Maps a {@link ResultSet} having 'id', 'username', 'full_name', 'password', 'enabled' to a {@link User} instance.
     * @param rs
     * @param ignored
     * @return a {@link User} object corresponding to the 'id' in the {@link ResultSet}
     * @throws SQLException
     */
    @Override
    public UserBean mapRow(ResultSet rs, int ignored) throws SQLException {
        int userId = rs.getInt("idUsuario");

        Collection<Role> roles = roleService.getRolesForUserId(userId);

        UserBean user = new UserBean(
                userId,
                rs.getString("usuario"),
                rs.getString("contrasena"),
                rs.getBoolean("enabled"),
                roles
                );

        int idEmpresa = rs.getInt("idEmpresa");
        logger.info("looking for empresa with id " + idEmpresa);
        Empresa empresa;

        if (idEmpresa == userId) {
            empresa = empresaService.getEmpresaForEmpresaUserId(rs.getInt("idEmpresa"), user);
        } else {
            empresa = empresaService.getEmpresaById(idEmpresa);
        }
        assert empresa != null;

        user.setEmpresa(empresa);

        return user;
    }
}
