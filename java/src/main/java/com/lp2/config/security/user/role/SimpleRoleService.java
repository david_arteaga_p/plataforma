package com.lp2.config.security.user.role;

import com.lp2.model.SqlHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by david on 3/9/17.
 */
@Service
public class SimpleRoleService implements RoleService {

    private final JdbcTemplate jdbcTemplate;
    private final RoleMapper roleMapper;

    @Autowired
    public SimpleRoleService(JdbcTemplate jdbcTemplate, RoleMapper roleMapper) {
        this.jdbcTemplate = jdbcTemplate;
        this.roleMapper = roleMapper;
    }

    @Override
    public Role getRoleForLabel(String roleName) {
        try {
            return jdbcTemplate.queryForObject("select * from Role where roleName = ?;", roleMapper, roleName);
        } catch (DataAccessException ex) {
            return null;
        }
    }

    @Override
    public Role getRoleForId(int roleId) {
        try {
            return jdbcTemplate.queryForObject("select * from Role where idRole = ?;", roleMapper, roleId);
        } catch (DataAccessException ex) {
            return null;
        }
    }

    @Override
    public List<Role> getAllRoles() {
        try {
            return jdbcTemplate.query("select * from Role;", roleMapper);
        } catch (DataAccessException ex) {
            return Collections.emptyList();
        }
    }

    @Override
    public Collection<Role> getRolesForUserId(int userId) {
        return jdbcTemplate.query("select Role.idRole as idRole, Role.roleName as roleName from Usuario_has_Roles, Usuario, Role where Usuario_has_Roles.idUsuario = Usuario.idUsuario and Usuario_has_Roles.idRole = Role.idRole and Usuario_has_Roles.idUsuario = ?;", roleMapper, userId);
    }

    @Override
    public void addUsersToRole(int roleId, List<Integer> userIds) {
        if (userIds.isEmpty()) {
            return;
        }

        String valuesSql = String.join(
                ", ",
                userIds.stream()
                        .map(userId -> "(" + userId + ", " + roleId + ")")
                        .collect(Collectors.toList())
        );
        jdbcTemplate.update(
                "insert ignore into Usuario_has_Roles values " + valuesSql + ";"
        );
    }

    @Override
    public void removeUsersFromRole(int roleId, List<Integer> userIds) {
        if (userIds.isEmpty()) {
            return;
        }

        jdbcTemplate.update(
                "delete from Usuario_has_Roles where idRole = " + roleId + " and "
                        + SqlHelper.joinColumnValuesBy(userIds, "idUsuario", "or") + ";");
    }
}
