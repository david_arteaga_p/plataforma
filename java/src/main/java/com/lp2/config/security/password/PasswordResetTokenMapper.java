package com.lp2.config.security.password;

import com.lp2.model.SqlHelper;
import com.lp2.plataforma.empresa.Empresa;
import com.lp2.plataforma.empresa.EmpresaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;

/**
 * Created by david on 6/26/17.
 */
@Component
public class PasswordResetTokenMapper implements RowMapper<PasswordResetToken> {

    private final EmpresaService empresaService;

    @Autowired
    public PasswordResetTokenMapper(EmpresaService empresaService) {
        this.empresaService = empresaService;
    }

    @Override
    public PasswordResetToken mapRow(ResultSet rs, int rowNum) throws SQLException {
        int idEmpresa = rs.getInt("idEmpresa");
        Empresa empresa = empresaService.getEmpresaById(idEmpresa);
        String token = rs.getString("token");
        LocalDateTime dateCreated = SqlHelper.getLocalDateTime(rs, "dateCreated");
        boolean valid = rs.getBoolean("valid");

        return new PasswordResetToken(empresa, token, dateCreated, valid);
    }
}
