package com.lp2.config.security.password;

import com.lp2.plataforma.empresa.Empresa;

/**
 * Created by david on 6/26/17.
 */
public interface PasswordResetService {
    boolean processPasswordResetRequest(String email, String appUrl);
    boolean validatePasswordResetToken(Empresa empresa, String token);
}
