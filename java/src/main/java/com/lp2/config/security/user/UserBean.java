package com.lp2.config.security.user;


import com.lp2.config.security.user.role.Role;
import com.lp2.plataforma.empresa.Empresa;

import java.util.Collection;

/**
 * Created by david on 3/4/17.
 */
public class UserBean implements User {

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserBean)) return false;

        UserBean userBean = (UserBean) o;

        return getId() == userBean.getId();
    }

    @Override
    public int hashCode() {
        return getId();
    }

    private int id;
    private String usuario;
    private String password;
    private boolean enabled;
    private Collection<Role> roles;
    private Empresa empresa;

    protected UserBean() {}

    UserBean(int id, String usuario, String password, boolean enabled, Collection<Role> roles, Empresa empresa) {
        this.id = id;
        this.usuario = usuario;
        this.password = password;
        this.enabled = enabled;
        this.roles = roles;
        this.empresa = empresa;
    }

    UserBean(int id, String usuario, String password, boolean enabled, Collection<Role> roles) {
        this.id = id;
        this.usuario = usuario;
        this.password = password;
        this.enabled = enabled;
        this.roles = roles;
    }

    protected UserBean(User user) {
        this.id = user.getId();
        this.usuario = user.getUsuario();
        this.password = user.getPassword();
        this.enabled = user.isEnabled();
        this.roles = user.getRoles();
    }

    @Override
    public String toString() {
        return "UserBean{" +
                "id=" + id +
                ", usuario='" + usuario + '\'' +
                ", password='" + password + '\'' +
                ", enabled=" + enabled +
                ", roles=" + roles +
                ", empresa=" + empresa +
                '}';
    }

    @Override
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    @Override
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public Collection<Role> getRoles() {
        return roles;
    }

    public void setRoles(Collection<Role> roles) {
        this.roles = roles;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }
}
