package com.lp2.config.security.user.role;

import java.util.Collection;
import java.util.List;

/**
 * Created by david on 3/11/17.
 */
public interface RoleService {
    Role getRoleForLabel(String roleName);

    Role getRoleForId(int roleId);

    List<Role> getAllRoles();

    Collection<Role> getRolesForUserId(int userId);

    void addUsersToRole(int roleId, List<Integer> userIds);

    void removeUsersFromRole(int roleId, List<Integer> userIds);
}
