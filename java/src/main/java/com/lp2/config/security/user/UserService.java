package com.lp2.config.security.user;

import com.lp2.plataforma.empresa.Empresa;
import com.lp2.web.form.EmpresaForm;

import java.util.List;

/**
 * Created by david on 3/1/17.
 */
public interface UserService {
    // search
    UserBean getUserById(int userId);
    UserBean getUserByUsername(String username);

    // CRUD
    UserBean createUser(NewUser user, Empresa empresa);
    void deleteUser(int userId);
    void disableUser(int userId);

    boolean changePassword(User user, String password);

}
