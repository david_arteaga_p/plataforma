package com.lp2.config.security.user;

import com.lp2.config.security.user.role.Role;
import com.lp2.config.security.user.role.RoleService;
import com.lp2.model.SqlHelper;
import com.lp2.plataforma.empresa.Empresa;
import com.mysql.jdbc.Statement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.sql.PreparedStatement;
import java.sql.Types;
import java.util.Collections;
import java.util.List;

/**
 * Created by david on 3/4/17.
 */
@Service
public class SimpleUserService implements UserService {

    private static final Logger logger = LoggerFactory.getLogger(SimpleUserService.class);

    private final JdbcTemplate jdbcTemplate;
    private final UserMapper userMapper;
    private final PasswordEncoder passwordEncoder;
    private final RoleService roleService;

    @Autowired
    public SimpleUserService(JdbcTemplate jdbcTemplate, UserMapper userMapper, PasswordEncoder passwordEncoder, RoleService roleService) {
        this.jdbcTemplate = jdbcTemplate;
        this.userMapper = userMapper;
        this.passwordEncoder = passwordEncoder;
        this.roleService = roleService;
    }

    @Override
    public UserBean getUserById(int userId) {
        try {
            return jdbcTemplate.queryForObject("select * from Usuario where idUsuario = ?;", userMapper, userId);
        } catch (DataAccessException ex) {
            logger.warn("Could not find user with id " + userId);
            return null;
        }
    }

    @Override
    public UserBean getUserByUsername(String username) {
        try {
            return jdbcTemplate.queryForObject("select * from Usuario where usuario = ?", userMapper, username);
        } catch (DataAccessException ex) {
            logger.info("user not found with name " + username);
            return null;
        }
    }



    @Override
    public UserBean createUser(NewUser user, Empresa empresa) {
        String encodedPassword = passwordEncoder.encode(user.getPassword());

        String sql = "insert into Usuario (usuario, contrasena, enabled, idEmpresa) values (?, ?, ?, ?);";

        KeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(con -> {
            PreparedStatement statement = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, user.getUsuario());
            statement.setString(2, encodedPassword);
            statement.setBoolean(3, user.isEnabled());
            if (empresa != null) {
                statement.setInt(4, empresa.getId());
            } else {
                statement.setNull(4, Types.INTEGER);
            }
            return statement;
        }, keyHolder);

        logger.info("user created with id " + keyHolder.getKey().intValue());

        Role userRole = roleService.getRoleForLabel("ROLE_USER");
        assert userRole != null;
        //give user role to new user
        jdbcTemplate.update(
                "insert into Usuario_has_Roles values (?, ?);",
                keyHolder.getKey().intValue(), userRole.getId());

        return new UserBean(
                keyHolder.getKey().intValue(),
                user.getUsuario(),
                encodedPassword,
                user.isEnabled(),
                Collections.singletonList(userRole),
                empresa);
    }

    @Override
    public void deleteUser(int userId) {
        jdbcTemplate.update("delete from Usuario where idUsuario = ?;", userId);
    }

    @Override
    public void disableUser(int userId) {
        //jdbcTemplate.update("update Usuario set enabled = true where id = ?;", userId);
    }

    @Override
    public boolean changePassword(User user, String password) {
        try {
            String encodedPassword = passwordEncoder.encode(password);

            int count = jdbcTemplate.update("update Usuario set contrasena = ? where idUsuario = ?", encodedPassword, user.getId());

            return count == 1;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

}
