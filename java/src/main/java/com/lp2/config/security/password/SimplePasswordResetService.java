package com.lp2.config.security.password;

import com.lp2.config.email.Email;
import com.lp2.plataforma.empresa.Empresa;
import com.lp2.plataforma.empresa.EmpresaService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.UUID;
import java.util.concurrent.ExecutorService;

/**
 * Created by david on 6/26/17.
 */
@Service
public class SimplePasswordResetService implements PasswordResetService {

    private static final Logger logger = LoggerFactory.getLogger(SimplePasswordResetService.class);

    private final EmpresaService empresaService;
    private final JdbcTemplate jdbcTemplate;
    private final JavaMailSender mailSender;
    private final PasswordResetTokenMapper passwordResetTokenMapper;

    @Autowired
    public SimplePasswordResetService(EmpresaService empresaService, JdbcTemplate jdbcTemplate, JavaMailSender mailSender, PasswordResetTokenMapper passwordResetTokenMapper) {
        this.empresaService = empresaService;
        this.jdbcTemplate = jdbcTemplate;
        this.mailSender = mailSender;
        this.passwordResetTokenMapper = passwordResetTokenMapper;
    }

    @Override
    public boolean processPasswordResetRequest(String email, String appUrl) {
        Empresa empresa = empresaService.getEmpresaByEmail(email);
        if (empresa == null) return true;

        String token = UUID.randomUUID().toString();
        if (!createPasswordResetToken(empresa, token)) {
            return false;
        }

        if (!sendPasswordResetEmail(empresa, token, appUrl)) {
            return false;
        }

        logger.info("sending email to " + email + " de empresa " + empresa.getRazonSocial());
        return true;
    }

    @Override
    public boolean validatePasswordResetToken(Empresa empresa, String token) {
        PasswordResetToken resetToken = getPasswordResetToken(empresa, token);

        if (resetToken == null) return false;

        if (!resetToken.isActive()) return false;

        if (resetToken.getDateCreated().isBefore(LocalDateTime.now().minusDays(1))) {
            invalidateResetToken(resetToken);
            return false;
        }

        if (!invalidateResetToken(resetToken)) return false;

        final Authentication auth = new UsernamePasswordAuthenticationToken(empresa.getUsuario(), null, Collections.singletonList(new SimpleGrantedAuthority("CHANGE_PASSWORD_PRIVILEGE")));
        SecurityContextHolder.getContext()
                .setAuthentication(auth);

        return true;
    }

    private PasswordResetToken getPasswordResetToken(Empresa empresa, String token) {
        try {
            return jdbcTemplate.queryForObject("select * from PasswordResetToken where idEmpresa = ? and token = ?", passwordResetTokenMapper, empresa.getId(), token);
        } catch (Exception e) {
            e.printStackTrace();
            logger.info("could not find token for empresa " + empresa.getId() + " token: " + token);
            return null;
        }
    }

    private boolean invalidateResetToken(PasswordResetToken token) {
        try {
            return 1 == jdbcTemplate.update("update PasswordResetToken set valid = false where idEmpresa = ? and token = ?", token.getEmpresa().getId(), token.getToken());
        } catch (Exception e) {
            logger.error("Could not invalidate token " + token);
            e.printStackTrace();
            return false;
        }
    }

    private static final ExecutorService executorService = java.util.concurrent.Executors.newCachedThreadPool();

    private boolean sendPasswordResetEmail(Empresa empresa, String token, String appUrl) {

        SimpleMailMessage email = constructResetTokenEmail(empresa, token, appUrl);

        executorService.submit(() -> {
            try {
                mailSender.send(email);
            } catch (Exception e) {
                logger.error("Could not send email");
                e.printStackTrace();
            }
        });

        return true;
    }

    private SimpleMailMessage constructResetTokenEmail(Empresa empresa, String token, String appUrl) {
        String url = String.format("%s/changePassword?id=%d&token=%s", appUrl, empresa.getId(), token);
        String message = "Haga click en el link para cambiar su contraseña";
        return constructEmail("Cambio de contraseña", message + " \r\n" + url, empresa.getEmail());
    }

    private SimpleMailMessage constructEmail(String subject, String body, String to) {
        SimpleMailMessage email = new SimpleMailMessage();
        email.setSubject(subject);
        email.setText(body);
        email.setTo(to);
        email.setFrom(Email.ADDRESS);
        return email;
    }

    private boolean createPasswordResetToken(Empresa empresa, String token) {
        try {
            return 1 == jdbcTemplate.update("insert into PasswordResetToken (idEmpresa, token, dateCreated) values (?, ?, ?)", empresa.getId(), token, LocalDateTime.now());
        } catch (Exception e) {
            logger.error("Could not create password reset token for empresa with id " + empresa.getId());
            e.printStackTrace();
            return false;
        }
    }
}
