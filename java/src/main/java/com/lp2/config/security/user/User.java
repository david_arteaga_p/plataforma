package com.lp2.config.security.user;

import com.lp2.config.security.user.role.Role;
import com.lp2.plataforma.empresa.Empresa;

import java.util.Collection;

/**
 * Created by david on 3/1/17.
 */
public interface User extends NewUser {
    int getId();
    Collection<Role> getRoles();
    Empresa getEmpresa();
}
