package com.lp2.config.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.thymeleaf.extras.springsecurity4.dialect.SpringSecurityDialect;

import static com.lp2.config.security.Authorities.CHANGE_PASSWORD_AUTHORITY;


/**
 * Created by david on 3/4/17.
 */
@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    UserAuthenticationProvider userAuthenticationProvider;

    @Autowired
    SimpleUserDetailsService simpleUserDetailsService;

    @Autowired
    public void config(AuthenticationManagerBuilder auth) throws Exception {
        auth
            .authenticationProvider(userAuthenticationProvider)
            .userDetailsService(simpleUserDetailsService);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            .authorizeRequests()
                .antMatchers("/assets/**").permitAll()
                .antMatchers("/register").permitAll()
                .antMatchers("/resetPassword").permitAll()
                .antMatchers("/changePassword").permitAll()
                .antMatchers("/changePasswordForm").hasAuthority(CHANGE_PASSWORD_AUTHORITY)
                .antMatchers("/admin/**").hasRole("ADMIN")
                .anyRequest().authenticated()
            .and()
                .formLogin().loginPage("/login").permitAll()
            .and()
                .logout().permitAll();
    }

    /**
     * Provides #authentication, #authorization, and sec:auth...
     * expressions and attributes in thymeleaf templates.
     * @return
     */
    @Bean
    public SpringSecurityDialect springSecurityDialect() {
        return new SpringSecurityDialect();
    }

}
