package com.lp2.web.form;

/**
 * Created by david on 6/17/17.
 */
public class MensajeForm {
    private String contenido;

    @Override
    public String toString() {
        return "MensajeForm{" +
                "contenido='" + contenido + '\'' +
                '}';
    }

    public String getContenido() {
        return contenido;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }

}
