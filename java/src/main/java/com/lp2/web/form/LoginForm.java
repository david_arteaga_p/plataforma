package com.lp2.web.form;

import com.form_generator.annotation.FormEntity;

/**
 * Created by david on 5/27/17.
 */
@FormEntity
public class LoginForm {
    private String usuario;
    private String password;

    public LoginForm() {
    }

    public LoginForm(String usuario, String password) {
        this.usuario = usuario;
        this.password = password;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}