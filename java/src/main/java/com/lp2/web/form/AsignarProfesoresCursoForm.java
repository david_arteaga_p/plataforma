package com.lp2.web.form;

import com.form_generator.annotation.FormEntity;
import com.form_generator.annotation.PredefinedType;
import com.lp2.plataforma.profesor.Profesor;

import java.util.List;

/**
 * Created by david on 6/6/17.
 */
@FormEntity
public class AsignarProfesoresCursoForm {
    @PredefinedType(idFieldName = "id", nameFieldName = "nombre") private List<Profesor> profesores;
}
