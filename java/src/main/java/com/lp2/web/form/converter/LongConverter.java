package com.lp2.web.form.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * Created by david on 6/10/17.
 */
@Component
public class LongConverter implements Converter<String, Long> {
    @Override
    public Long convert(String source) {
        try {
            return Long.parseLong(source);
        } catch (Exception e) {
            return null;
        }
    }
}
