package com.lp2.web.form.converter.horario;

import com.lp2.plataforma.horario.Horario;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.time.DayOfWeek;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by david on 6/20/17.
 */
@Component
public class HorarioConverter implements Converter<String, Horario> {

    // th:id="'slot_' + ${dia.num} + '_' + ${hora.inicio} + '_' + ${hora.fin}"

    private final static Pattern pattern = Pattern.compile("^(slot_)" +
            "([1-7])" + // day of week
            "(_)" +
            "([0-2][0-9]|[0-9])"); // hora

    @Override
    public Horario convert(String source) {
        Matcher matcher;
        if ((matcher = pattern.matcher(source)).matches()) {
            DayOfWeek dia = DayOfWeek.of(Integer.parseInt(matcher.group(2)));
            int hora = Integer.parseInt(matcher.group(4));
            return new Horario(dia, hora, true);
        }

        return null;
    }
}
