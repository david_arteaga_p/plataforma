package com.lp2.web.form;

import com.form_generator.annotation.FormEntity;
import com.form_generator.annotation.FormHidden;
import com.lp2.plataforma.profesor.Profesor;

/**
 * Created by david on 6/6/17.
 */
@FormEntity
public class ProfesorForm {
    @FormHidden private Integer id;
    private String nombre;
    private String apellido;
    private Integer dni;
    private String email;
    private Long telefono;
    private String usuario;
    private String password;

    public static ProfesorForm from(Profesor profesor) {

        ProfesorForm profesorForm = new ProfesorForm();

        profesorForm.id = profesor.getId();
        profesorForm.nombre = profesor.getNombre();
        profesorForm.apellido = profesor.getApellido();
        profesorForm.dni = profesor.getDni();
        profesorForm.email = profesor.getEmail();
        profesorForm.telefono = profesor.getTelefono();

        return profesorForm;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public Integer getDni() {
        return dni;
    }

    public void setDni(Integer dni) {
        this.dni = dni;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getTelefono() {
        return telefono;
    }

    public void setTelefono(Long telefono) {
        this.telefono = telefono;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
