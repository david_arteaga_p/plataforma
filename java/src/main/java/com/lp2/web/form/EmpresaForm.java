package com.lp2.web.form;

import com.form_generator.annotation.FormEntity;
import com.form_generator.annotation.FormHidden;

/**
 * Created by david on 6/6/17.
 */
@FormEntity
public class EmpresaForm {
    @FormHidden private Integer id;
    private String razonSocial;
    private Long ruc;
    private String rubro;
    private Long telefono;
    private String email;
    private String usuario;
    private String password;
    private String descripcion;

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public Long getTelefono() {
        return telefono;
    }

    public void setTelefono(Long telefono) {
        this.telefono = telefono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRubro() {
        return rubro;
    }

    public void setRubro(String rubro) {
        this.rubro = rubro;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return "EmpresaForm{" +
                "id=" + id +
                ", razonSocial='" + razonSocial + '\'' +
                ", ruc=" + ruc +
                ", rubro='" + rubro + '\'' +
                ", telefono=" + telefono +
                ", email='" + email + '\'' +
                ", usuario='" + usuario + '\'' +
                ", password='" + password + '\'' +
                ", descripcion='" + descripcion + '\'' +
                '}';
    }

    public Long getRuc() {
        return ruc;
    }

    public void setRuc(Long ruc) {
        this.ruc = ruc;
    }
}
