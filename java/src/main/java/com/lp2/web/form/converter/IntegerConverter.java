package com.lp2.web.form.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * Created by david on 6/6/17.
 */
@Component
public class IntegerConverter implements Converter<String, Integer> {
    @Override
    public Integer convert(String source) {
        try {
            return Integer.parseInt(source);
        } catch (Exception e) {
            return null;
        }
    }
}
