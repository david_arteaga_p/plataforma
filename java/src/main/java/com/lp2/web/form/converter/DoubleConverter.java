package com.lp2.web.form.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * Created by david on 6/6/17.
 */
@Component
public class DoubleConverter implements Converter<String, Double> {
    @Override
    public Double convert(String source) {
        try {
            return Double.parseDouble(source);
        } catch (Exception e) {
            return null;
        }
    }
}
