package com.lp2.web.form.converter.annotation;

import org.springframework.format.annotation.DateTimeFormat;

import java.lang.annotation.*;

/**
 * Annotation to format a date field for an input tag of type date
 * Created by david on 6/7/17.
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER, ElementType.ANNOTATION_TYPE})
@DateTimeFormat(pattern = "yyyy-MM-dd")
public @interface DateInput {
}
