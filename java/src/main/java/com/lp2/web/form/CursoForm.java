package com.lp2.web.form;

import com.form_generator.annotation.FormEntity;
import com.form_generator.annotation.FormHidden;
import com.lp2.plataforma.curso.CursoCore;
import com.lp2.web.form.converter.annotation.DateInput;

import java.time.LocalDate;

/**
 * Created by david on 6/6/17.
 */
@FormEntity
public class CursoForm {
    @FormHidden private Integer id;
    private String nombre;
    private String sumilla;
    @DateInput
    private LocalDate fechaInicio;
    @DateInput
    private LocalDate fechaFin;
    private Integer horasDuracion;
    private Integer vacantes;
    private Double costo;
    private boolean activo;

    public static CursoForm from(CursoCore cursoCore) {
        CursoForm cursoForm = new CursoForm();
        cursoForm.id = cursoCore.getId();
        cursoForm.nombre = cursoCore.getNombre();
        cursoForm.sumilla = cursoCore.getSumilla();
        cursoForm.fechaInicio = cursoCore.getFechaInicio();
        cursoForm.fechaFin = cursoCore.getFechaFin();
        cursoForm.horasDuracion = cursoCore.getHorasDuracion();
        cursoForm.vacantes = cursoCore.getVacantes();
        cursoForm.costo = cursoCore.getCosto();
        cursoForm.activo = cursoCore.isActivo();

        return cursoForm;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getSumilla() {
        return sumilla;
    }

    public void setSumilla(String sumilla) {
        this.sumilla = sumilla;
    }

    public LocalDate getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(LocalDate fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public LocalDate getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(LocalDate fechaFin) {
        this.fechaFin = fechaFin;
    }

    public Integer getHorasDuracion() {
        return horasDuracion;
    }

    public void setHorasDuracion(Integer horasDuracion) {
        this.horasDuracion = horasDuracion;
    }

    public Integer getVacantes() {
        return vacantes;
    }

    public void setVacantes(Integer vacantes) {
        this.vacantes = vacantes;
    }

    public Double getCosto() {
        return costo;
    }

    public void setCosto(Double costo) {
        this.costo = costo;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }


    @Override
    public String toString() {
        return "CursoForm{" +
                "id=" + id +
                ", nombre='" + nombre + '\'' +
                ", sumilla='" + sumilla + '\'' +
                ", fechaInicio=" + fechaInicio +
                ", fechaFin=" + fechaFin +
                ", horasDuracion=" + horasDuracion +
                ", vacantes=" + vacantes +
                ", costo=" + costo +
                ", activo=" + activo +
                '}';
    }
}
