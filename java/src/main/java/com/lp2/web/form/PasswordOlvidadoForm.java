package com.lp2.web.form;

import com.form_generator.annotation.FormEntity;

/**
 * Created by david on 6/6/17.
 */
@FormEntity
public class PasswordOlvidadoForm {
    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
