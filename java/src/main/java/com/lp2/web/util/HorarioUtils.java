package com.lp2.web.util;

import com.lp2.plataforma.horario.util.Dia;
import com.lp2.plataforma.horario.util.Hora;

import java.time.DayOfWeek;
import java.time.format.TextStyle;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by david on 6/21/17.
 */
public class HorarioUtils {
    public final static List<Dia> dias = Arrays.stream(DayOfWeek.values())
            .map(d -> new Dia(d.getDisplayName(TextStyle.FULL, Locale.forLanguageTag("es")), d.getValue()))
            .collect(Collectors.toList());
    public final static List<Hora> horas = IntStream.range(0, 24).boxed().map(start -> new Hora(start, start + 1)).collect(Collectors.toList());
}
