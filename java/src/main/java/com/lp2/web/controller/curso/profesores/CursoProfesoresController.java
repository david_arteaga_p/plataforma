package com.lp2.web.controller.curso.profesores;

import com.lp2.plataforma.curso.Curso;
import com.lp2.plataforma.curso.CursoCore;
import com.lp2.plataforma.curso.CursoService;
import com.lp2.plataforma.empresa.Empresa;
import com.lp2.plataforma.empresa.EmpresaService;
import com.lp2.plataforma.horario.Horario;
import com.lp2.plataforma.horario.curso.HorarioCurso;
import com.lp2.plataforma.horario.curso.HorarioCursoService;
import com.lp2.plataforma.profesor.Profesor;
import com.lp2.plataforma.profesor.ProfesorService;
import com.lp2.web.form.converter.horario.HorarioConverter;
import com.lp2.web.util.HorarioUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.security.Principal;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by david on 6/21/17.
 */
@Controller
public class CursoProfesoresController {

    private static final Logger logger = LoggerFactory.getLogger(CursoProfesoresController.class);

    private final CursoService cursoService;
    private final ProfesorService profesorService;
    private final EmpresaService empresaService;
    private final HorarioCursoService horarioCursoService;
    private final HorarioConverter horarioConverter;

    @Autowired
    public CursoProfesoresController(CursoService cursoService, ProfesorService profesorService, EmpresaService empresaService, HorarioCursoService horarioCursoService, HorarioConverter horarioConverter) {
        this.cursoService = cursoService;
        this.profesorService = profesorService;
        this.empresaService = empresaService;
        this.horarioCursoService = horarioCursoService;
        this.horarioConverter = horarioConverter;
    }

    @GetMapping("/curso/{idCurso:[0-9]+}/profesores")
    public ModelAndView editarProfesoresCursoForm(ModelMap model, @PathVariable("idCurso") int idCurso, Principal principal) {

        Empresa empresa = empresaService.getEmpresaByUsername(principal.getName());

        CursoCore cursoCore;

        if ((cursoCore = cursoService.getCursoCoreById(idCurso)) != null) {
            List<Profesor> profesoresCurso = profesorService.getProfesoresForCurso(cursoCore, empresa);

            Curso curso = new Curso(cursoCore, profesoresCurso, null, null, empresa);

            model.addAttribute("curso", curso);

            List<Profesor> profesoresEmpresa = profesorService.getProfesoresForEmpresa(empresa);
            model.addAttribute("profesoresEmpresa", profesoresEmpresa);

            model.addAttribute("dias", HorarioUtils.dias);
            model.addAttribute("horas", HorarioUtils.horas);

        } else {
            model.addAttribute("error", "No existe un curso con id " + idCurso);
        }

        return new ModelAndView("curso/admin/editarProfesoresCurso", model);
    }

    @GetMapping("/curso/{idCurso:[0-9]+}/profesoresDisponibles")
    @ResponseBody
    public ResponseEntity<List<ProfesorAsignado>> getProfesoresAsignados(@PathVariable("idCurso") int idCurso, Principal principal) {
        CursoCore cursoCore;
        if ((cursoCore = cursoService.getCursoCoreById(idCurso)) == null) {
            return ResponseEntity.badRequest().body(null);
        }

        Empresa empresa = empresaService.getEmpresaByUsername(principal.getName());

        List<HorarioCurso> horariosCursos = horarioCursoService.getHorariosForCurso(cursoCore);

        Map<Integer, List<HorarioCurso>> horariosPorProfesor = horariosCursos.stream()
                .collect(Collectors.groupingBy(h -> h.getProfesor().getId(), Collectors.toList()));

        List<ProfesorAsignado> profesorAsignados = profesorService.getProfesoresForEmpresa(empresa).stream()
                .map(p -> new ProfesorAsignado(p, horariosPorProfesor.getOrDefault(p.getId(), Collections.emptyList())))
                .collect(Collectors.toList());

        return ResponseEntity.ok(profesorAsignados);
    }

    @PostMapping("/curso/{idCurso:[0-9]+}/profesores/agregar")
    @ResponseBody
    public ResponseEntity agregarProfesorCurso(@PathVariable("idCurso") int idCurso, @RequestParam("idProfesor") int idProfesor) throws Exception {

        CursoCore cursoCore = cursoService.getCursoCoreById(idCurso);
        Profesor profesor = profesorService.getProfesorById(idProfesor);

        if (cursoCore != null && profesor != null) {
            if (!cursoService.agregarProfesorCurso(profesor, cursoCore)) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("No se pudo agregar al profesor. Asegurese de que no esté ya agregado.");
            }
        } else {
            StringBuilder stringBuilder = new StringBuilder();
            if (cursoCore == null) {
                stringBuilder.append("No existe un curso con id ").append(idCurso).append(". ");
            }
            if (profesor == null) {
                stringBuilder.append("No existe un profesor con id ").append(idProfesor).append(". ");
            }
            return ResponseEntity.badRequest().body(stringBuilder.toString());
        }
        return ResponseEntity.ok(new ProfesorAgregadoResponse(cursoCore.getId(), profesor.getId(),profesor.getNombre() + ' ' + profesor.getApellido(), "Profesor agregado"));
    }

    @PostMapping("/curso/{idCurso:[0-9]+}/profesores/eliminar")
    @ResponseBody
    public ResponseEntity<String> eliminarProfesorCurso(@PathVariable("idCurso") int idCurso, @RequestParam("idProfesor") int idProfesor) throws Exception {
        CursoCore cursoCore = cursoService.getCursoCoreById(idCurso);
        Profesor profesor = profesorService.getProfesorById(idProfesor);

        if (cursoCore != null && profesor != null) {
            // curso y profesor válidos
            if (!cursoService.eliminarProfesorCurso(profesor, cursoCore)) {
                // no se pudo eliminar al profesor
                throw new Exception("No se pudo eliminar al profesor");
            }
            return ResponseEntity.ok("Profesor eliminado");
        } else {
            // alguno de los ids no son válidos
            StringBuilder stringBuilder = new StringBuilder();
            if (cursoCore == null) {
                stringBuilder.append("No existe un curso con id ").append(idCurso).append(". ");
            }
            if (profesor == null) {
                stringBuilder.append("No existe un profesor con id ").append(idProfesor).append(". ");
            }
            return ResponseEntity.badRequest().body(stringBuilder.toString());
        }
    }

    @PostMapping("/curso/{idCurso:[0-9]+}/agregarSlotProfesor")
    @ResponseBody
    public ResponseEntity<String> agregarSlotProfesor(
            @PathVariable("idCurso") int idCurso,
            @RequestParam("idProfesor") int idProfesor,
            @RequestParam("idSlot") String idSlot
    ) {
        logger.info(String.format("idCurso: %d; idProfesor %d; idSlot: %s", idCurso, idProfesor, idSlot));

        //TODO
        CursoCore cursoCore = cursoService.getCursoCoreById(idCurso);
        if (cursoCore == null) {
            return ResponseEntity.badRequest().body("Curso no existe con id " + idCurso);
        }

        Profesor profesor = profesorService.getProfesorById(idProfesor);
        if (profesor == null) {
            return ResponseEntity.badRequest().body("Profesor no existe con id " + idProfesor);
        }

        Horario horario = horarioConverter.convert(idSlot);
        if (horario == null) {
            return ResponseEntity.badRequest().body("idSlot no es válido: " + idSlot);
        }

        if (!horarioCursoService.agregarHorarioCursoProfesor(cursoCore, profesor, horario)) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Could not save right now. :(");
        }

        return ResponseEntity.ok("Profesor agregado en slot " + idSlot);
    }

    @PostMapping("/curso/{idCurso:[0-9]+}/eliminarSlotProfesor")
    @ResponseBody
    public ResponseEntity<String> eliminarSlotProfesor(
            @PathVariable("idCurso") int idCurso,
            @RequestParam("idProfesor") int idProfesor,
            @RequestParam("idSlot") String idSlot
    ) {
        logger.info(String.format("idCurso: %d; idProfesor %d; idSlot: %s", idCurso, idProfesor, idSlot));

        //TODO
        CursoCore cursoCore = cursoService.getCursoCoreById(idCurso);
        if (cursoCore == null) {
            return ResponseEntity.badRequest().body("Curso no existe con id " + idCurso);
        }

        Profesor profesor = profesorService.getProfesorById(idProfesor);
        if (profesor == null) {
            return ResponseEntity.badRequest().body("Profesor no existe con id " + idProfesor);
        }

        Horario horario = horarioConverter.convert(idSlot);
        if (horario == null) {
            return ResponseEntity.badRequest().body("idSlot no es válido: " + idSlot);
        }

        if (!horarioCursoService.eliminarHorarioCursoProfesor(cursoCore, profesor, horario)) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Could not save right now. :(");
        }

        return ResponseEntity.ok("Profesor eliminado de slot " + idSlot);
    }

}
