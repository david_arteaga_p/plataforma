package com.lp2.web.controller.asigna;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.view.RedirectView;

/**
 * Created by david on 4/25/17.
 */
@Controller
public class AsignarController {

    private static Logger logger = LoggerFactory.getLogger(AsignarController.class);

    @GetMapping("/asigna/create")
    public ModelAndView asignaCreateForm(ModelMap model) {


        return new ModelAndView("asignarProfesoresCurso", model);
    }

    @PostMapping("/asigna/create")
    public View profesorCreate() {
        return new RedirectView("/asigna/create");
    }

}
