package com.lp2.web.controller.mensaje;

import com.lp2.plataforma.curso.CursoCore;
import com.lp2.plataforma.curso.CursoService;
import com.lp2.plataforma.empresa.Empresa;
import com.lp2.plataforma.empresa.EmpresaService;
import com.lp2.plataforma.mensaje.MensajeService;
import com.lp2.plataforma.pregunta.Pregunta;
import com.lp2.plataforma.pregunta.PreguntaService;
import com.lp2.web.form.MensajeForm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import java.security.Principal;

/**
 * Created by david on 6/17/17.
 */
@Controller
public class MensajeController {

    private static final Logger logger = LoggerFactory.getLogger(MensajeController.class);

    private final CursoService cursoService;
    private final PreguntaService preguntaService;
    private final MensajeService mensajeService;
    private final EmpresaService empresaService;

    @Autowired
    public MensajeController(CursoService cursoService, PreguntaService preguntaService, MensajeService mensajeService, EmpresaService empresaService) {
        this.cursoService = cursoService;
        this.preguntaService = preguntaService;
        this.mensajeService = mensajeService;
        this.empresaService = empresaService;
    }


    @PostMapping("/curso/{idCurso:[0-9]+}/pregunta/{idPregunta:[0-9]+}/agregarMensaje")
    public View agregarPregunta(
            @PathVariable("idCurso") int idCurso,
            @PathVariable("idPregunta") int idPregunta,
            MensajeForm mensajeForm,
            Principal principal,
            RedirectAttributes attributes) {

        CursoCore cursoCore;
        if ((cursoCore = cursoService.getCursoCoreById(idCurso )) == null) {
            attributes.addFlashAttribute("error", "No existe un curso con id " + idCurso);
            return new RedirectView("/cursos");
        }

        Pregunta pregunta;
        if ((pregunta = preguntaService.getPreguntaById(idPregunta, cursoCore)) == null) {
            attributes.addFlashAttribute("error", "No existe una pregunta con id " + idPregunta);
            return new RedirectView(String.format("/curso/%d/preguntas", idCurso));
        }

        logger.info(mensajeForm.toString());
        Empresa empresa = empresaService.getEmpresaByUsername(principal.getName());
        if (mensajeService.agregarMensajePregunta(mensajeForm, pregunta, empresa) == null) {
            attributes.addFlashAttribute("error", "No se pudo enviar su mensaje.");
        }


        return new RedirectView(String.format("/curso/%d/pregunta/%d", idCurso, idPregunta));
    }

}
