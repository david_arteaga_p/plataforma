package com.lp2.web.controller.report;

import com.lp2.plataforma.curso.CursoCore;
import com.lp2.plataforma.curso.CursoService;
import com.lp2.plataforma.empresa.Empresa;
import com.lp2.plataforma.empresa.EmpresaService;
import com.lp2.plataforma.horario.curso.HorarioCurso;
import com.lp2.plataforma.horario.curso.HorarioCursoService;
import com.lp2.plataforma.profesor.ProfesorService;
import com.lp2.plataforma.report.ReporteProfesoresCursos;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.view.document.AbstractPdfView;

import java.security.Principal;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by david on 6/26/17.
 */
@Controller
public class ReportController {


    private final CursoService cursoService;
    private final ProfesorService profesorService;
    private final HorarioCursoService horarioCursoService;
    private final EmpresaService empresaService;

    @Autowired
    public ReportController(CursoService cursoService, ProfesorService profesorService, HorarioCursoService horarioCursoService, EmpresaService empresaService) {
        this.cursoService = cursoService;
        this.profesorService = profesorService;
        this.horarioCursoService = horarioCursoService;
        this.empresaService = empresaService;
    }


    @GetMapping("/reporte.pdf")
    public AbstractPdfView reporte(ModelMap model, Principal principal) {
        model.addAttribute("key prueba", "valor prueba");

        Empresa empresa = empresaService.getEmpresaByUsername(principal.getName());

        List<CursoCore> cursos = cursoService.getCursosForEmpresa(empresa);

        Map<Integer, List<HorarioCurso>> horariosPorCurso = cursos.stream()
                .collect(Collectors.toMap(curso -> curso.getId(), curso -> horarioCursoService.getHorariosForCurso(curso)));


        return new ReporteProfesoresCursos(cursos, horariosPorCurso);
    }
}
