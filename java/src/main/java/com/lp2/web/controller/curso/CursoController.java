package com.lp2.web.controller.curso;

import com.lp2.config.security.user.UserService;
import com.lp2.plataforma.curso.CursoCore;
import com.lp2.plataforma.curso.CursoService;
import com.lp2.plataforma.empresa.Empresa;
import com.lp2.plataforma.empresa.EmpresaService;
import com.lp2.web.form.CursoForm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;

/**
 * Created by david on 4/25/17.
 */
@Controller
public class CursoController {

    private static Logger logger = LoggerFactory.getLogger(CursoController.class);

    private final UserService userService;
    private final CursoService cursoService;
    private final EmpresaService empresaService;

    @Autowired
    public CursoController(UserService userService, CursoService cursoService, EmpresaService empresaService) {
        this.userService = userService;
        this.cursoService = cursoService;
        this.empresaService = empresaService;
    }

    @GetMapping("/curso/create")
    public ModelAndView cursoCreateForm(ModelMap model) {


        return new ModelAndView("curso/registrarCurso", model);
    }

    @PostMapping("/curso/create")
    public View cursoCreate(
            CursoForm cursoForm,
            @RequestParam("imagenCurso") MultipartFile imagenCurso,
            Principal principal,
            RedirectAttributes attributes) {

        Empresa empresa = empresaService.getEmpresaByUsername(principal.getName());

        CursoCore cursoCore = cursoService.createCurso(cursoForm, empresa);
        attributes.addFlashAttribute("message", "Curso '" + cursoForm.getNombre() + "' creado");

        if (!imagenCurso.isEmpty()) {
            if (!cursoService.guardarImagen(cursoCore, imagenCurso)) {
                attributes.addFlashAttribute("error", "No se pudo guardar la imagen");
            }
        }

        return new RedirectView("/curso/" + cursoCore.getId());
    }

    @GetMapping("/curso/{idCurso:[0-9]+}/editar")
    public ModelAndView editarCursoForm(@PathVariable("idCurso") int idCurso, ModelMap model) {
        CursoCore cursoCore;
        if ((cursoCore = cursoService.getCursoCoreById(idCurso)) != null) {
            model.addAttribute("curso", CursoForm.from(cursoCore));
        } else {
            model.addAttribute("error", "No existe un curso con id " + idCurso);
        }

        return new ModelAndView("curso/editarCurso", model);
    }

    @PostMapping("/curso/{idCurso:[0-9]+}/editar")
    public View editarCurso(@PathVariable("idCurso") int idCurso, ModelMap model, CursoForm cursoForm, RedirectAttributes attributes) {
        assert idCurso == cursoForm.getId();
        if (cursoService.getCursoCoreById(idCurso) != null) {
            cursoService.editarCurso(cursoForm);
            attributes.addFlashAttribute("message", "Curso '" + cursoForm.getNombre() + "' guardado");
        } else {
            attributes.addFlashAttribute("error", "No existe un curso con id " + idCurso);
            return new RedirectView("/cursos");
        }

        return new RedirectView("/curso/" + idCurso);
    }

    @GetMapping("/cursos")
    public ModelAndView viewCursos(ModelMap model, Principal principal, HttpServletRequest request) {
        Empresa empresa = empresaService.getEmpresaByUsername(principal.getName());
        model.addAttribute("cursos", cursoService.getCursosForEmpresa(empresa));

        return new ModelAndView("curso/verCursos", model);
    }

    @GetMapping("/curso/{idCurso:[0-9]+}/imagen")
    public ResponseEntity<byte[]> viewImage(@PathVariable("idCurso") int idCurso) {
        CursoCore cursoCore = cursoService.getCursoCoreById(idCurso);
        if (cursoCore == null) {
            return ResponseEntity.notFound().build();
        }

        byte[] imageData = cursoService.getImagenForCurso(cursoCore);
        if (imageData == null) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok(imageData);
    }

}
