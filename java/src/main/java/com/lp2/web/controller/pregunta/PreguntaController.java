package com.lp2.web.controller.pregunta;

import com.lp2.plataforma.curso.Curso;
import com.lp2.plataforma.curso.CursoCore;
import com.lp2.plataforma.curso.CursoService;
import com.lp2.plataforma.empresa.Empresa;
import com.lp2.plataforma.empresa.EmpresaService;
import com.lp2.plataforma.pregunta.Pregunta;
import com.lp2.plataforma.pregunta.PreguntaService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.ModelAndView;

import java.security.Principal;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by david on 4/25/17.
 */
@Controller
public class PreguntaController {

    private static Logger logger = LoggerFactory.getLogger(PreguntaController.class);

    private final CursoService cursoService;
    private final EmpresaService empresaService;
    private final PreguntaService preguntaService;

    @Autowired
    public PreguntaController(CursoService cursoService, EmpresaService empresaService, PreguntaService preguntaService) {
        this.cursoService = cursoService;
        this.empresaService = empresaService;
        this.preguntaService = preguntaService;
    }

    @GetMapping("/preguntas")
    public ModelAndView verPreguntas(ModelMap model, Principal principal) {
        Empresa empresa = empresaService.getEmpresaByUsername(principal.getName());

        List<CursoCore> cursosCore = cursoService.getCursosForEmpresa(empresa);
        List<Curso> cursos = cursosCore.stream()
                .map(cc -> new Curso(cc, null, preguntaService.getPreguntasForCurso(cc), null, empresa))
                .collect(Collectors.toList());

        model.addAttribute("cursos", cursos);

        return new ModelAndView("pregunta/verPreguntas", model);
    }

    @GetMapping("/curso/{idCurso:[0-9]+}/preguntas")
    public ModelAndView verPreguntasCurso(
            ModelMap model,
            @PathVariable("idCurso") int idCurso) {

        CursoCore cursoCore;
        if ((cursoCore = cursoService.getCursoCoreById(idCurso)) == null) {
            model.addAttribute("error", "No existe un curso con id " + idCurso);
        } else {
            model.addAttribute("preguntas", preguntaService.getPreguntasForCurso(cursoCore));
            model.addAttribute("curso", cursoCore);
        }

        return new ModelAndView("pregunta/verPreguntasCurso", model);
    }

    @GetMapping("/curso/{idCurso:[0-9]+}/pregunta/{idPregunta:[0-9]+}")
    public ModelAndView verPregunta(@PathVariable("idCurso") int idCurso, @PathVariable("idPregunta") int idPregunta, ModelMap model, Principal principal) {

        ModelAndView returnView = new ModelAndView("pregunta/verPregunta", model);

        CursoCore cursoCore;
        if ((cursoCore = cursoService.getCursoCoreById(idCurso)) == null) {
            model.addAttribute("error", "No existe un curso con id " + idCurso);
            return returnView;
        }

        Pregunta pregunta;
        if ((pregunta = preguntaService.getPreguntaById(idPregunta, cursoCore)) == null) {
            model.addAttribute("error", "No existe una poregunta con id " + idPregunta);
            return returnView;
        }

        Empresa empresa = empresaService.getEmpresaByUsername(principal.getName());
        model.addAttribute("empresa", empresa);

        model.addAttribute("pregunta", pregunta);

        preguntaService.setRead(pregunta, empresa);

        return returnView;
    }

}
