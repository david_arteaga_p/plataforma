package com.lp2.web.controller.alumno;

import com.lp2.plataforma.alumno.Alumno;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.view.RedirectView;

/**
 * Created by david on 4/25/17.
 */
@Controller
public class AlumnoController {

    private static Logger logger = LoggerFactory.getLogger(AlumnoController.class);

    @GetMapping("/alumno/create")
    public ModelAndView alumnoCreateForm(ModelMap model) {

        return new ModelAndView("registrarAlumno", model);
    }

    @PostMapping("alumno/create")
    public View alumnoCreate() {
        return new RedirectView("/alumno/create");
    }
}
