package com.lp2.web.controller.profesor;


import com.lp2.config.security.user.UserBean;
import com.lp2.config.security.user.UserService;
import com.lp2.plataforma.empresa.Empresa;
import com.lp2.plataforma.empresa.EmpresaService;
import com.lp2.plataforma.horario.Horario;
import com.lp2.plataforma.horario.profesor.HorarioProfesorService;
import com.lp2.plataforma.profesor.Profesor;
import com.lp2.plataforma.profesor.ProfesorService;
import com.lp2.web.form.ProfesorForm;
import com.lp2.web.form.converter.horario.HorarioConverter;
import com.lp2.web.util.HorarioUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import java.security.Principal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by david on 4/25/17.
 */
@Controller
public class ProfesorController {

    private static Logger logger = LoggerFactory.getLogger(ProfesorController.class);

    private final ProfesorService profesorService;
    private final EmpresaService empresaService;
    private final UserService userService;
    private final HorarioProfesorService horarioProfesorService;
    private final HorarioConverter horarioConverter;

    @Autowired
    public ProfesorController(ProfesorService profesorService, EmpresaService empresaService, UserService userService, HorarioProfesorService horarioProfesorService, HorarioConverter horarioConverter) {
        this.profesorService = profesorService;
        this.empresaService = empresaService;
        this.userService = userService;
        this.horarioProfesorService = horarioProfesorService;
        this.horarioConverter = horarioConverter;
    }

    @GetMapping("/profesores")
    public ModelAndView viewProfesores(ModelMap model, Principal principal) {
        Empresa empresa = empresaService.getEmpresaByUsername(principal.getName());
        model.addAttribute("profesores", profesorService.getProfesoresForEmpresa(empresa));

        return new ModelAndView("profesor/verProfesores", model);
    }

    @GetMapping("/profesor/create")
    public ModelAndView profesorCreateForm(ModelMap model) {
        if (!model.containsAttribute("profesor")) {
            model.addAttribute("profesor", new ProfesorForm());
        }

        return new ModelAndView("profesor/registrarProfesor", model);
    }

    @PostMapping("/profesor/create")
    public View profesorCreate(RedirectAttributes attributes, Principal principal, ProfesorForm profesorForm) {
        Empresa empresa = empresaService.getEmpresaByUsername(principal.getName());
        UserBean user;

        // verificar que no haya un usuario con el mismo nombre
        if ((user = userService.getUserByUsername(profesorForm.getUsuario())) != null && user.getEmpresa().getId() == empresa.getId()) {
            attributes.addFlashAttribute("error", "Ya existe el usuario '" + profesorForm.getUsuario() + "'");
            attributes.addFlashAttribute("profesor", profesorForm);
            return new RedirectView("/profesor/create");
        }

        Profesor profesor = profesorService.createProfesor(profesorForm, empresa);
        attributes.addFlashAttribute("message", "Profesor registrado");

        return new RedirectView("/profesor/" + profesor.getId());
    }

    @GetMapping("/profesor/{idProfesor:[0-9]+}/editar")
    public ModelAndView editarProfesorForm(@PathVariable("idProfesor") int idProfesor, ModelMap model) {
        Profesor profesor;
        if ((profesor = profesorService.getProfesorById(idProfesor)) != null) {
            model.addAttribute("profesor", ProfesorForm.from(profesor));
        } else {
            model.addAttribute("error", "No existe un profesor con id " + idProfesor);
        }

        return new ModelAndView("profesor/editarProfesor", model);
    }

    @PostMapping("/profesor/{idProfesor:[0-9]+}/editar")
    public View editarProfesor(
            @PathVariable("idProfesor") int idProfesor,
            ModelMap model,
            ProfesorForm profesorForm,
            RedirectAttributes attributes) {

        assert idProfesor == profesorForm.getId();

        if (profesorService.getProfesorById(idProfesor) != null) {
            profesorService.editarProfesor(profesorForm);
            attributes.addFlashAttribute("message", "Datos guardados");
        } else {
            attributes.addFlashAttribute("error", "No existe un profesor con id " + idProfesor);
            return new RedirectView("/profesores");
        }

        return new RedirectView("/profesor/" + idProfesor);
    }

    @GetMapping("/profesor/{idProfesor:[0-9]+}")
    public ModelAndView verProfesor(ModelMap model, @PathVariable("idProfesor") int idProfesor) {
        Profesor profesor = profesorService.getProfesorById(idProfesor);

        if (profesor != null) {
            model.addAttribute("profesor", profesor);
            model.addAttribute("dias", HorarioUtils.dias);
            model.addAttribute("horas", HorarioUtils.horas);
        } else {
            model.addAttribute("error", "No existe un profesor con id " + idProfesor);
        }

        return new ModelAndView("profesor/verProfesor", model);
    }

    @PostMapping("/profesor/{idProfesor:[0-9]+}/guardarHorario")
    public View agregarHorarios(RedirectAttributes attributes, @PathVariable("idProfesor") int idProfesor, @RequestBody MultiValueMap<String, Object> map) {

        Profesor profesor;
        if ((profesor = profesorService.getProfesorById(idProfesor)) != null) {
            logger.info(map.toString());
            List<Horario> horarios = map.keySet().stream().map(horarioConverter::convert).filter(Objects::nonNull).collect(Collectors.toList());
            logger.info(horarios.toString());
            logger.info("map len: " + map.keySet().size() + "; horarios: " + horarios.size());
            horarioProfesorService.updateHorariosProfesor(profesor, horarios);
        } else {
            attributes.addFlashAttribute("error", "No existe un profesor con id " + idProfesor);
        }

        return new RedirectView("/profesor/" + idProfesor);
    }

}