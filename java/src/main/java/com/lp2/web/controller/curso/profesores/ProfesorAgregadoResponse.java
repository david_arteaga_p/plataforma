package com.lp2.web.controller.curso.profesores;

/**
 * Created by david on 6/22/17.
 */
class ProfesorAgregadoResponse {
    private int idCurso;
    private int idProfesor;
    private String nombreProfesor;
    private String message;

    public int getIdCurso() {
        return idCurso;
    }

    public void setIdCurso(int idCurso) {
        this.idCurso = idCurso;
    }

    public int getIdProfesor() {
        return idProfesor;
    }

    public void setIdProfesor(int idProfesor) {
        this.idProfesor = idProfesor;
    }

    public String getNombreProfesor() {
        return nombreProfesor;
    }

    public void setNombreProfesor(String nombreProfesor) {
        this.nombreProfesor = nombreProfesor;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ProfesorAgregadoResponse(int idCurso, int idProfesor, String nombreProfesor, String message) {

        this.idCurso = idCurso;
        this.idProfesor = idProfesor;
        this.nombreProfesor = nombreProfesor;
        this.message = message;
    }
}