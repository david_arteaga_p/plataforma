package com.lp2.web.controller.password;

import com.lp2.config.security.password.PasswordResetService;
import com.lp2.config.security.user.UserService;
import com.lp2.plataforma.empresa.Empresa;
import com.lp2.plataforma.empresa.EmpresaService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;

/**
 * Created by david on 6/26/17.
 */

@Controller
public class ResetPasswordController {

    private static final Logger logger = LoggerFactory.getLogger(ResetPasswordController.class);

    private final PasswordResetService passwordResetService;
    private final EmpresaService empresaService;
    private final UserService userService;

    @Autowired
    public ResetPasswordController(PasswordResetService passwordResetService, EmpresaService empresaService, UserService userService) {
        this.passwordResetService = passwordResetService;
        this.empresaService = empresaService;
        this.userService = userService;
    }

    @PostMapping("/resetPassword")
    public View resetPassword(@RequestParam("email") String email, RedirectAttributes attributes, HttpServletRequest request) {

        if (!passwordResetService.processPasswordResetRequest(email, getAppUrl(request))) {
            attributes.addFlashAttribute("error", "No se pudo enviar el correo. Porfavor intentelo otra vez");
            return new RedirectView("/resetPassword");
        }

        attributes.addFlashAttribute("message", "Se ha enviado un correo a la dirección " + email);

        return new RedirectView("/login");
    }

    private String getAppUrl(HttpServletRequest request) {
        return "http://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
    }

    @GetMapping("/changePassword")
    public View changePasswordValidateToken(@RequestParam("id") int idEmpresa, @RequestParam("token") String token, RedirectAttributes attributes) {

        Empresa empresa = empresaService.getEmpresaById(idEmpresa);
        if (empresa == null) {
            attributes.addFlashAttribute("error", "Id incorrecto");
            return new RedirectView("/login");
        }

        if (!passwordResetService.validatePasswordResetToken(empresa, token)) {
            attributes.addFlashAttribute("error", "El link ya no es válido");
            return new RedirectView("/login");
        }


        return new RedirectView("/changePasswordForm");
    }

    @PostMapping("/changePassword")
    public View changePassword(@RequestParam("password") String password, RedirectAttributes attributes, Principal principal, HttpServletRequest request) {
        Empresa empresa = empresaService.getEmpresaByUsername(principal.getName());

        logger.info("changing password for " + empresa.getId() + " to pass: " + password);

        if (!userService.changePassword(empresa, password)) {
            attributes.addFlashAttribute("error", "No se pudo cambiar la contraseña.");
        } else {
            attributes.addFlashAttribute("message", "Su contraseña ha sido cambiada.");
        }

        SecurityContextHolder.getContext()
                .setAuthentication(null);

        return new RedirectView("/login");
    }

    @GetMapping("/changePasswordForm")
    public ModelAndView changePasswordForm(Principal principal, ModelMap model) {
        logger.info(principal.getName());
        model.addAttribute("razonSocial", empresaService.getEmpresaByUsername(principal.getName()).getRazonSocial());
        return new ModelAndView("home/changePasswordForm");
    }
}
