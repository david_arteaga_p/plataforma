package com.lp2.web.controller.profesor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.view.RedirectView;



/**
 * Created by david on 4/25/17.
 */
@Controller
public class PerfilProfesorController {
    private static Logger logger = LoggerFactory.getLogger(PerfilProfesorController.class);

    @GetMapping("/pProfesor/create")
    public ModelAndView loginCreateForm(ModelMap model) {


        return new ModelAndView("perfilDeProfesor", model);
    }

    @PostMapping("/pProfesor/create")
    public View pProfesorCreate() {
        return new RedirectView("/pProfesor/create");
    }
}
