package com.lp2.web.controller.home;

import com.lp2.config.security.user.User;
import com.lp2.config.security.user.UserService;
import com.lp2.plataforma.empresa.Empresa;
import com.lp2.plataforma.empresa.EmpresaService;
import com.lp2.web.form.EmpresaForm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by david on 3/9/17.
 */
@Controller
@RequestMapping("/register")
public class RegistrationController {

    private final static Logger logger = LoggerFactory.getLogger(RegistrationController.class);

    private final UserService userService;
    private final AuthenticationManager authenticationManager;
    private final EmpresaService empresaService;

    @Autowired
    public RegistrationController(UserService userService, AuthenticationManager authenticationManager, EmpresaService empresaService) {
        this.userService = userService;
        this.authenticationManager = authenticationManager;
        this.empresaService = empresaService;
    }

    @GetMapping
    public String registratonForm(Model model) {
        if (!model.containsAttribute("newUser")) {
            model.addAttribute("newUser", new EmpresaForm());
        }

        return "home/register";
    }

    @PostMapping
    public View registerNewUser(
            EmpresaForm empresaForm,
            RedirectAttributes attributes,
            HttpServletRequest request
    ) {

        logger.info(empresaForm.toString());

        User user = userService.getUserByUsername(empresaForm.getUsuario());

        if (user != null) {
            attributes.addFlashAttribute("newUser", empresaForm);
            attributes.addFlashAttribute("error", "El usuario '" + empresaForm.getUsuario() + "' ya existe");
            return new RedirectView("/register");
        }

        Empresa empresa = empresaService.getEmpresaByEmail(empresaForm.getEmail());
        if (empresa != null) {
            attributes.addFlashAttribute("newUser", empresaForm);
            attributes.addFlashAttribute("error", "El correo '" + empresaForm.getEmail() + "' ya está asociado a otra cuenta");
            return new RedirectView("/register");
        }

        user = empresaService.createEmpresa(empresaForm);

        authenticateUserAndSetSession(user, request);

        attributes.addFlashAttribute("message", "Ha sido registrado");

        return new RedirectView("/cursos");
    }

    private void authenticateUserAndSetSession(User user, HttpServletRequest request) {
        String username = user.getUsuario();
        String password = user.getPassword();
        UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(username, password);

        // generate session if one doesn't exist
        request.getSession();

        token.setDetails(new WebAuthenticationDetails(request));
        Authentication authenticatedUser = authenticationManager.authenticate(token);

        SecurityContextHolder.getContext().setAuthentication(authenticatedUser);
    }

}
