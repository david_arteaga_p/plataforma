package com.lp2.web.controller.empresa;

import com.lp2.plataforma.empresa.Empresa;
import com.lp2.plataforma.empresa.EmpresaService;
import com.lp2.web.form.EmpresaForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import java.security.Principal;

/**
 * Created by david on 6/24/17.
 */
@Controller
public class EmpresaPerfilController {

    private final EmpresaService empresaService;

    @Autowired
    public EmpresaPerfilController(EmpresaService empresaService) {
        this.empresaService = empresaService;
    }

    @GetMapping("/perfil")
    public ModelAndView verPerfilEmpresa(Principal principal, ModelMap model) {
        Empresa empresa = empresaService.getEmpresaByUsername(principal.getName());

        model.addAttribute("empresa", empresa);

        return new ModelAndView("/empresa/verPerfil", model);
    }

    @GetMapping("/perfil/editar")
    public ModelAndView editarEmpresaForm(Principal principal, ModelMap model) {
        Empresa empresa = empresaService.getEmpresaByUsername(principal.getName());

        model.addAttribute("empresa", empresa);

        return new ModelAndView("/empresa/editarPerfil", model);
    }

    @PostMapping("/perfil/editar")
    public View editarEmpresa(Principal principal, RedirectAttributes attributes, EmpresaForm empresaForm) {

        Empresa empresa = empresaService.getEmpresaByUsername(principal.getName());

        if (empresaService.editarEmpresa(empresa, empresaForm)) {
            attributes.addFlashAttribute("message", "Los datos han sido guardados");
        } else {
            attributes.addFlashAttribute("error", "No se pudieron guardar los datos.");
        }

        return new RedirectView("/perfil");
    }
}
