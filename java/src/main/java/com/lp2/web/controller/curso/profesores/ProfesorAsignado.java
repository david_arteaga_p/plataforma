package com.lp2.web.controller.curso.profesores;

import com.lp2.plataforma.curso.Curso;
import com.lp2.plataforma.horario.curso.HorarioCurso;
import com.lp2.plataforma.horario.profesor.HorarioProfesor;
import com.lp2.plataforma.profesor.Profesor;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by david on 6/22/17.
 */
public class ProfesorAsignado {

    private int idProfesor;
    private String nombre;
    private List<String> horariosDisponiblesParaCurso;
    private List<String> horariosAsignadosAlCurso;

    private boolean cargado = false;

    public ProfesorAsignado(Profesor profesor, List<HorarioCurso> horariosCursoProfesor) {
        this.idProfesor = profesor.getId();
        this.nombre = profesor.getNombre();
        this.horariosDisponiblesParaCurso = profesor.getHorarios().stream()
                .filter(HorarioProfesor::isDisponible)
                .map(HorarioProfesor::getStringId)
                .collect(Collectors.toList());
        this.horariosAsignadosAlCurso = horariosCursoProfesor.stream()
                .map(HorarioCurso::getStringId)
                .collect(Collectors.toList());
    }

    public int getIdProfesor() {
        return idProfesor;
    }

    public void setIdProfesor(int idProfesor) {
        this.idProfesor = idProfesor;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<String> getHorariosDisponiblesParaCurso() {
        return horariosDisponiblesParaCurso;
    }

    public void setHorariosDisponiblesParaCurso(List<String> horariosDisponiblesParaCurso) {
        this.horariosDisponiblesParaCurso = horariosDisponiblesParaCurso;
    }

    public List<String> getHorariosAsignadosAlCurso() {
        return horariosAsignadosAlCurso;
    }

    public void setHorariosAsignadosAlCurso(List<String> horariosAsignadosAlCurso) {
        this.horariosAsignadosAlCurso = horariosAsignadosAlCurso;
    }

    public boolean isCargado() {
        return cargado;
    }

    public void setCargado(boolean cargado) {
        this.cargado = cargado;
    }
}
