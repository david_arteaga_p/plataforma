package com.lp2.web.controller.alumnosInscritos;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.view.RedirectView;

/**
 * Created by david on 4/25/17.
 */
@Controller
public class AlumnosInscritosController {

    private static Logger logger = LoggerFactory.getLogger(com.lp2.web.controller.alumnosInscritos.AlumnosInscritosController.class);

    @GetMapping("/alumnosInscritos/create")
    public ModelAndView loginCreateForm(ModelMap model) {


        return new ModelAndView("Ver alumnos inscritos", model);
    }

    @PostMapping("/alumnosInscritos/create")
    public View alumnosInscritosCreate() {
        return new RedirectView("/alumnosInscritos/create");
    }

}
