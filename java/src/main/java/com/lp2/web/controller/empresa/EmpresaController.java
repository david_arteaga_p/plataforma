package com.lp2.web.controller.empresa;

import com.lp2.plataforma.empresa.Empresa;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.view.RedirectView;

/**
 * Created by david on 4/25/17.
 */
@Controller
public class EmpresaController {

    private static Logger logger = LoggerFactory.getLogger(EmpresaController.class);

    @GetMapping("/empresa/create")
    public ModelAndView EmpresaCreateForm(ModelMap model) {
        return new ModelAndView("registrarEmpresa", model);
    }

    @PostMapping("/empresa/create")
    public View empresaCreate() {
        return new RedirectView("/empresa/create");
    }
}
