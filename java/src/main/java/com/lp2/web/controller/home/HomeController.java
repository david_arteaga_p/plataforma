package com.lp2.web.controller.home;

import com.lp2.config.security.password.PasswordResetService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by david on 3/8/17.
 */
@Controller
public class HomeController {

    private static final Logger logger = LoggerFactory.getLogger(HomeController.class);

    @GetMapping("/")
    public View home() {
        return new RedirectView("/cursos");
    }

    @GetMapping("/login")
    public String login() {
        return "home/login";
    }

    @GetMapping("/resetPassword")
    public ModelAndView resetPasswordForm() {
        return new ModelAndView("home/resetPassword");
    }



}
