package com.lp2.web.controller.curso;

import com.lp2.plataforma.alumno.Alumno;
import com.lp2.plataforma.alumno.AlumnoService;
import com.lp2.plataforma.curso.Curso;
import com.lp2.plataforma.curso.CursoCore;
import com.lp2.plataforma.curso.CursoService;
import com.lp2.plataforma.empresa.Empresa;
import com.lp2.plataforma.empresa.EmpresaService;
import com.lp2.plataforma.pregunta.Pregunta;
import com.lp2.plataforma.pregunta.PreguntaService;
import com.lp2.plataforma.profesor.Profesor;
import com.lp2.plataforma.profesor.ProfesorService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.security.Principal;
import java.util.List;

/**
 * Created by david on 6/10/17.
 */
@Controller
public class CursoAdminController {

    private static final Logger logger = LoggerFactory.getLogger(CursoAdminController.class);

    private final CursoService cursoService;
    private final ProfesorService profesorService;
    private final EmpresaService empresaService;
    private final PreguntaService preguntaService;
    private final AlumnoService alumnoService;

    @Autowired
    public CursoAdminController(CursoService cursoService, ProfesorService profesorService, EmpresaService empresaService, PreguntaService preguntaService, AlumnoService alumnoService) {
        this.cursoService = cursoService;
        this.profesorService = profesorService;
        this.empresaService = empresaService;
        this.preguntaService = preguntaService;
        this.alumnoService = alumnoService;
    }

    @GetMapping("/curso/{idCurso:[0-9]+}")
    public ModelAndView adminCurso(
            ModelMap model,
            @PathVariable("idCurso") int idCurso,
            Principal principal) {
        CursoCore cursoCore;
        if ((cursoCore = cursoService.getCursoCoreById(idCurso)) != null) {

            Empresa empresa = empresaService.getEmpresaByUsername(principal.getName());

            List<Profesor> profesores = profesorService.getProfesoresForCurso(cursoCore, empresa);
            List<Pregunta> preguntas = preguntaService.getPreguntasForCurso(cursoCore);
            List<Alumno> alumnos = alumnoService.getAlumnosForCurso(cursoCore);

            Curso curso = new Curso(cursoCore, profesores, preguntas, alumnos, empresa);


            model.addAttribute("curso", curso);
        } else {
            model.addAttribute("error", "No existe un curso con id " + idCurso);
        }

        return new ModelAndView("curso/admin/adminCurso", model);
    }






}

