package com.lp2.plataforma.alumno;

import com.lp2.plataforma.curso.CursoCore;

import java.util.List;

/**
 * Created by david on 6/17/17.
 */
public interface AlumnoService {
    Alumno getAlumnoById(int idAlumno);
    List<Alumno> getAlumnosForCurso(CursoCore cursoCore);
}
