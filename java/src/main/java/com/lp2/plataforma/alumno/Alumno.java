package com.lp2.plataforma.alumno;

import com.lp2.config.security.user.User;
import com.lp2.config.security.user.UserBean;
import com.lp2.plataforma.curso.CursoCore;

import java.util.List;

/**
 * Created by david on 4/8/17.
 */
public class Alumno extends UserBean {

    private String email;
    private String nombre;
    private String apellido;

    Alumno(User user, String email, String nombre, String apellido) {
        super(user);
        this.email = email;
        this.nombre = nombre;
        this.apellido = apellido;
    }

    @Override
    public String toString() {
        return "Alumno{" +
                "email='" + email + '\'' +
                ", nombre='" + nombre + '\'' +
                ", apellido='" + apellido + '\'' +
                '}';
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }
}
