package com.lp2.plataforma.alumno;


import com.lp2.config.security.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by david on 6/17/17.
 */
@Component
public class AlumnoMapper implements RowMapper<Alumno> {

    private final UserService userService;

    @Autowired
    public AlumnoMapper(UserService userService) {
        this.userService = userService;
    }

    @Override
    public Alumno mapRow(ResultSet rs, int rowNum) throws SQLException {
        String email = rs.getString("email");
        String nombre = rs.getString("nombre");
        String apellido = rs.getString("apellido");
        return new Alumno(userService.getUserById(rs.getInt("Usuario_idUsuario")), email, nombre, apellido);
    }
}
