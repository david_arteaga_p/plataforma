package com.lp2.plataforma.alumno;

import com.lp2.plataforma.curso.CursoCore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by david on 6/17/17.
 */
@Service
public class SimpleAlumnoService implements AlumnoService {

    private final AlumnoMapper alumnoMapper;
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public SimpleAlumnoService(AlumnoMapper alumnoMapper, JdbcTemplate jdbcTemplate) {
        this.alumnoMapper = alumnoMapper;
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Alumno getAlumnoById(int idAlumno) {
        try {
            return jdbcTemplate.queryForObject("select * from Alumno where Usuario_idUsuario = ?", alumnoMapper, idAlumno);
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public List<Alumno> getAlumnosForCurso(CursoCore cursoCore) {
        List<Integer> ids = jdbcTemplate.queryForList("select Alumno_Usuario_idUsuario from Curso_has_Alumno where Curso_idCurso = ?", Integer.class, cursoCore.getId());
        return ids.stream()
                .map(this::getAlumnoById)
                .collect(Collectors.toList());
    }
}
