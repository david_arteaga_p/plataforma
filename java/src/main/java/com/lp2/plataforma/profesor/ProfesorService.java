package com.lp2.plataforma.profesor;

import com.lp2.config.security.user.User;
import com.lp2.plataforma.empresa.Empresa;
import com.lp2.plataforma.curso.CursoCore;
import com.lp2.web.form.ProfesorForm;

import java.util.List;

/**
 * Created by david on 6/10/17.
 */
public interface ProfesorService {
    Profesor getProfesorForUser(User user);
    List<Profesor> getProfesoresForCurso(CursoCore cursoCore, Empresa empresa);
    Profesor createProfesor(ProfesorForm profesorForm, Empresa empresa);

    List<Profesor> getProfesoresForEmpresa(Empresa empresa);

    Profesor getProfesorById(int idProfesor);

    void editarProfesor(ProfesorForm profesorForm);
}
