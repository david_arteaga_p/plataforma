package com.lp2.plataforma.profesor;

import com.lp2.config.security.user.User;
import com.lp2.config.security.user.UserService;
import com.lp2.plataforma.empresa.Empresa;
import com.lp2.plataforma.empresa.EmpresaService;
import com.lp2.plataforma.horario.profesor.HorarioProfesorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by david on 6/10/17.
 */
@Service
public class ProfesorMapper implements RowMapper<Profesor> {

    private final UserService userService;
    private final EmpresaService empresaService;
    private final HorarioProfesorService horarioProfesorService;

    @Autowired
    public ProfesorMapper(UserService userService, EmpresaService empresaService, HorarioProfesorService horarioProfesorService) {
        this.userService = userService;
        this.empresaService = empresaService;
        this.horarioProfesorService = horarioProfesorService;
    }

    @Override
    public Profesor mapRow(ResultSet rs, int rowNum) throws SQLException {
        int id = rs.getInt("id");
        int dni = rs.getInt("dni");
        String nombre = rs.getString("nombre");
        String apellido = rs.getString("apellido");
        Long telefono = (Long) rs.getObject("telefono");
        String email = rs.getString("email");

        User user = userService.getUserById(id);

        int idEmpresa = rs.getInt("Empresa_Usuario_idUsuario");
        User empresaUser = userService.getUserById(idEmpresa);

        Empresa empresa = empresaService.getEmpresaForEmpresaUserId(idEmpresa, empresaUser);

        return new Profesor(user, nombre, apellido, dni, email, telefono, empresa, horarioProfesorService);
    }
}
