package com.lp2.plataforma.profesor;

import com.lp2.config.security.user.User;
import com.lp2.config.security.user.UserBean;
import com.lp2.plataforma.empresa.Empresa;
import com.lp2.plataforma.empresa.EmpresaService;
import com.lp2.plataforma.horario.profesor.HorarioProfesor;
import com.lp2.plataforma.horario.profesor.HorarioProfesorService;
import com.lp2.web.form.ProfesorForm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by david on 4/7/17.
 */
public class Profesor extends UserBean {


    private final static Logger logger = LoggerFactory.getLogger(Profesor.class);

    private String nombre;
    private String apellido;
    private int dni;
    private String email;
    private Long telefono;
    private Empresa empresa;
    private List<HorarioProfesor> horarios;

    public boolean ocupado(int numDia, int horaInicio) {
        return horarios.stream()
                .anyMatch(horario -> horario.getDia().getValue() == numDia && horaInicio == horario.getHora()
                        && !horario.isDisponible());
    }

    public boolean tieneHorario(int numDia, int horaInicio) {
        return horarios.stream()
                .anyMatch(horario -> horario.getDia().getValue() == numDia && horaInicio == horario.getHora());
    }

    Profesor(User user, String nombre, String apellido, int dni, String email, Long telefono, Empresa empresa, HorarioProfesorService horarioProfesorService) {
        super(user);
        this.nombre = nombre;
        this.apellido = apellido;
        this.dni = dni;
        this.email = email;
        this.telefono = telefono;
        this.empresa = empresa;
        if (horarioProfesorService != null) {
            this.horarios = horarioProfesorService.getHorariosForProfesor(this);
            logger.info(this.horarios.toString());
        }
    }

    /**
     * Used when returning a newly created Profesor
     * @param user
     * @param profesorForm
     * @param empresa
     */
    Profesor(User user, ProfesorForm profesorForm, Empresa empresa) {
        this(user, profesorForm.getNombre(), profesorForm.getApellido(), profesorForm.getDni(),
                profesorForm.getEmail(), profesorForm.getTelefono(), empresa, null);
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public int getDni() {
        return dni;
    }

    public void setDni(int dni) {
        this.dni = dni;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getTelefono() {
        return telefono;
    }

    public void setTelefono(Long telefono) {
        this.telefono = telefono;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public List<HorarioProfesor> getHorarios() {
        return horarios;
    }

    public void setHorarios(List<HorarioProfesor> horarios) {
        this.horarios = horarios;
    }

    @Override
    public String toString() {
        return "Profesor{" +
                "nombre='" + nombre + '\'' +
                ", apellido='" + apellido + '\'' +
                ", dni=" + dni +
                ", email='" + email + '\'' +
                ", telefono=" + telefono +
                ", empresa=" + empresa +
                ", horarios=" + horarios +
                "} " + super.toString();
    }
}
