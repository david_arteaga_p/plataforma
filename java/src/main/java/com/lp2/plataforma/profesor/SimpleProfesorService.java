package com.lp2.plataforma.profesor;

import com.lp2.config.security.user.NewUser;
import com.lp2.config.security.user.User;
import com.lp2.config.security.user.UserService;
import com.lp2.model.SqlHelper;
import com.lp2.plataforma.empresa.Empresa;
import com.lp2.plataforma.curso.CursoCore;
import com.lp2.web.form.ProfesorForm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Service;

import java.sql.PreparedStatement;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by david on 6/10/17.
 */
@Service
public class SimpleProfesorService implements ProfesorService {

    private static final Logger logger = LoggerFactory.getLogger(SimpleProfesorService.class);

    private final JdbcTemplate jdbcTemplate;
    private final ProfesorMapper profesorMapper;
    private final UserService userService;

    @Autowired
    public SimpleProfesorService(JdbcTemplate jdbcTemplate, ProfesorMapper profesorMapper, UserService userService) {
        this.jdbcTemplate = jdbcTemplate;
        this.profesorMapper = profesorMapper;
        this.userService = userService;
    }

    @Override
    public Profesor getProfesorForUser(User user) {
        return jdbcTemplate.queryForObject("select * from Profesor where idUsuario = ?", profesorMapper, user.getId());
    }

    @Override
    public List<Profesor> getProfesoresForCurso(CursoCore cursoCore, Empresa empresa) {
        List<Integer> profesoresIds = jdbcTemplate.queryForList("select Profesor_idProfesor from Curso_has_Profesor where Curso_idCurso = ?", Integer.class, cursoCore.getId());
        return profesoresIds.stream()
                .map(this::getProfesorById)
                .collect(Collectors.toList());
    }

    @Override
    public Profesor createProfesor(ProfesorForm profesorForm, Empresa empresa) {
        User user = userService.createUser(new NewUser() {
            @Override
            public String getUsuario() {
                return profesorForm.getUsuario();
            }

            @Override
            public String getPassword() {
                return profesorForm.getPassword();
            }

            @Override
            public boolean isEnabled() {
                return false;
            }
        }, empresa);



        KeyHolder keyHolder = new GeneratedKeyHolder();

        String sql = "insert into Profesor (id, dni, nombre, apellido, Empresa_Usuario_idUsuario, telefono, email) values (? ,? ,? ,? ,? ,? ,?)";

        jdbcTemplate.update(con -> {
            PreparedStatement statement = con.prepareStatement(sql);

            statement.setInt(1, user.getId());
            statement.setInt(2, profesorForm.getDni());
            statement.setString(3, profesorForm.getNombre());
            statement.setString(4, profesorForm.getApellido());
            statement.setInt(5, empresa.getId());
            SqlHelper.setLong(statement, 6, profesorForm.getTelefono());
            statement.setString(7, profesorForm.getEmail());

            return statement;

        });

        profesorForm.setId(user.getId());

        return new Profesor(user, profesorForm, empresa);
    }

    @Override
    public List<Profesor> getProfesoresForEmpresa(Empresa empresa) {
        return jdbcTemplate.query("select * from Profesor where Empresa_Usuario_idUsuario = ?", profesorMapper, empresa.getId());
    }

    @Override
    public Profesor getProfesorById(int idProfesor) {
        try {
            return jdbcTemplate.queryForObject("select * from Profesor where id = ?", profesorMapper, idProfesor);
        } catch (Exception e) {
            logger.info("could not find profesor with id " + idProfesor);
            return null;
        }

    }

    @Override
    public void editarProfesor(ProfesorForm profesorForm) {
        String sql = "update Profesor set dni = ?, nombre = ?, apellido = ?, telefono = ?, email = ? where id = ?";

        jdbcTemplate.update(con -> {
            PreparedStatement statement = con.prepareStatement(sql);

            statement.setInt(1, profesorForm.getDni());
            statement.setString(2, profesorForm.getNombre());
            statement.setString(3, profesorForm.getApellido());
            SqlHelper.setLong(statement, 4, profesorForm.getTelefono());
            statement.setString(5, profesorForm.getEmail());

            statement.setInt(6, profesorForm.getId());

            return statement;

        });
    }
}
