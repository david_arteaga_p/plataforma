package com.lp2.plataforma.pregunta;

import com.lp2.plataforma.alumno.Alumno;
import com.lp2.plataforma.alumno.AlumnoService;
import com.lp2.plataforma.curso.CursoCore;
import com.lp2.plataforma.mensaje.Mensaje;
import com.lp2.plataforma.profesor.Profesor;
import com.lp2.plataforma.mensaje.MensajeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by david on 5/9/17.
 */
public class PreguntaMapper implements RowMapper<Pregunta> {

    private final static Logger logger = LoggerFactory.getLogger(PreguntaMapper.class);

    private final CursoCore cursoCore;
    private final MensajeService mensajeService;
    private final AlumnoService alumnoService;


    public PreguntaMapper(CursoCore cursoCore, MensajeService mensajeService, AlumnoService alumnoService) {
        this.cursoCore = cursoCore;
        this.mensajeService = mensajeService;
        this.alumnoService = alumnoService;
    }

    /**
     * Implementations must implement this method to map each row of data
     * in the ResultSet. This method should not call {@code next()} on
     * the ResultSet; it is only supposed to map values of the current row.
     *
     * @param rs     the ResultSet to map (pre-initialized for the current row)
     * @param rowNum the number of the current row
     * @return the result object for the current row
     * @throws SQLException if a SQLException is encountered getting
     *                      column values (that is, there's no need to catch SQLException)
     */
    @Override
    public Pregunta mapRow(ResultSet rs, int rowNum) throws SQLException {

        int idPregunta = rs.getInt("idPreguntas");
        String titulo = rs.getString("titulo");

        int idAlumno = rs.getInt("Alumno_Usuario_idUsuario");
        Alumno alumno = alumnoService.getAlumnoById(idAlumno);

        // TODO
        Profesor profesor = null;

        Pregunta pregunta = new Pregunta(idPregunta, titulo, alumno, profesor, cursoCore, mensajeService);

        return pregunta;

    }
}
