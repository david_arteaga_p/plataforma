package com.lp2.plataforma.pregunta;

import com.lp2.config.security.user.User;
import com.lp2.plataforma.alumno.AlumnoService;
import com.lp2.plataforma.curso.CursoCore;
import com.lp2.plataforma.mensaje.Mensaje;
import com.lp2.plataforma.mensaje.MensajeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by david on 5/9/17.
 */
@Service
public class SimplePreguntaService implements PreguntaService {

    private final JdbcTemplate jdbcTemplate;
    private final MensajeService mensajeService;
    private final AlumnoService alumnoService;

    @Autowired
    public SimplePreguntaService(JdbcTemplate jdbcTemplate, MensajeService mensajeService, AlumnoService alumnoService) {
        this.jdbcTemplate = jdbcTemplate;
        this.mensajeService = mensajeService;
        this.alumnoService = alumnoService;
    }


    @Override
    public List<Pregunta> getPreguntasForCurso(CursoCore cursoCore) {
        PreguntaMapper preguntaMapper = new PreguntaMapper(cursoCore, mensajeService, alumnoService);

        return jdbcTemplate.query("select * from Preguntas where Curso_idCurso = ?", preguntaMapper, cursoCore.getId());
    }

    @Override
    public Pregunta getPreguntaById(int idPregunta, CursoCore cursoCore) {
        PreguntaMapper preguntaMapper = new PreguntaMapper(cursoCore, mensajeService, alumnoService);

        try {
            return jdbcTemplate.queryForObject("select * from Preguntas where idPreguntas = ?", preguntaMapper, idPregunta);
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public void setRead(Pregunta pregunta, User user) {
        pregunta.getMensajes().stream()
                .filter(m -> m.getDestinatario().getId() == user.getId())
                .forEach(mensajeService::setRead);
    }
}
