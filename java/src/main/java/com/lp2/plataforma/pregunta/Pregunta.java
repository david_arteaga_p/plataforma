package com.lp2.plataforma.pregunta;

import com.lp2.plataforma.alumno.Alumno;
import com.lp2.plataforma.curso.CursoCore;
import com.lp2.plataforma.mensaje.Mensaje;
import com.lp2.plataforma.mensaje.MensajeService;
import com.lp2.plataforma.profesor.Profesor;

import java.util.Collections;
import java.util.List;

/**
 * Created by david on 4/8/17.
 */
public class Pregunta {

    private int id;
    private String titulo;
    private Alumno alumno;
    private Profesor profesor;
    private CursoCore cursoCore;
    private List<Mensaje> mensajes;

    public boolean mensajeEsPropio(Mensaje mensaje) {
        return mensaje.getRemitente().getId() == cursoCore.getEmpresa().getId();
    }

    public long getCantidadMensajesNoLeidos() {
        return mensajes.stream().filter(m -> !m.isLeido()).count();
    }

    Pregunta(int id, String titulo, Alumno alumno, Profesor profesor, CursoCore cursoCore, MensajeService mensajeService) {
        this.id = id;
        this.titulo = titulo;
        this.alumno = alumno;
        this.profesor = profesor;
        this.cursoCore = cursoCore;
        this.mensajes = mensajeService.getMensajesForPregunta(this);
        this.mensajes.sort((right, left) -> left.getId() < right.getId() ? -1 : (right.getId() < left.getId() ? 1 : 0));
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public Alumno getAlumno() {
        return alumno;
    }

    public void setAlumno(Alumno alumno) {
        this.alumno = alumno;
    }

    public Profesor getProfesor() {
        return profesor;
    }

    public void setProfesor(Profesor profesor) {
        this.profesor = profesor;
    }

    public CursoCore getCursoCore() {
        return cursoCore;
    }

    public void setCursoCore(CursoCore cursoCore) {
        this.cursoCore = cursoCore;
    }

    public List<Mensaje> getMensajes() {
        return mensajes;
    }

    public void setMensajes(List<Mensaje> mensajes) {
        this.mensajes = mensajes;
    }

    @Override
    public String toString() {
        return "Pregunta{" +
                "id=" + id +
                ", titulo='" + titulo + '\'' +
                ", alumno=" + alumno +
                ", profesor=" + profesor +
                ", cursoCore=" + cursoCore +
                ", mensajes=" + mensajes +
                '}';
    }
}
