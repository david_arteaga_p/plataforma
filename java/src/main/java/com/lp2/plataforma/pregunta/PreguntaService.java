package com.lp2.plataforma.pregunta;

import com.lp2.config.security.user.User;
import com.lp2.plataforma.curso.CursoCore;

import java.util.List;

/**
 * Created by david on 5/9/17.
 */
public interface PreguntaService {
    List<Pregunta> getPreguntasForCurso(CursoCore cursoCore);

    Pregunta getPreguntaById(int idPregunta, CursoCore cursoCore);

    void setRead(Pregunta pregunta, User user);
}
