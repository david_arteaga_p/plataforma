package com.lp2.plataforma.horario.curso;

import com.lp2.plataforma.curso.Curso;
import com.lp2.plataforma.curso.CursoCore;
import com.lp2.plataforma.horario.Horario;
import com.lp2.plataforma.profesor.Profesor;

import java.time.DayOfWeek;

/**
 * Created by david on 6/20/17.
 */
public class HorarioCurso extends Horario {
    private int idHorarioCurso;
    private CursoCore cursoCore;
    private Profesor profesor;

    public HorarioCurso(int idHorarioCurso, CursoCore cursoCore, Profesor profesor, DayOfWeek dia, int hora, boolean disponible) {
        super(dia, hora, disponible);
        this.idHorarioCurso = idHorarioCurso;
        this.cursoCore = cursoCore;
        this.profesor = profesor;
    }



    @Override
    public String toString() {
        return "HorarioCurso{" +
                "idHorarioCurso=" + idHorarioCurso +
                ", cursoCore=" + cursoCore +
                ", profesor=" + profesor +
                "} " + super.toString();
    }

    public int getIdHorarioCurso() {
        return idHorarioCurso;
    }

    public void setIdHorarioCurso(int idHorarioCurso) {
        this.idHorarioCurso = idHorarioCurso;
    }

    public CursoCore getCursoCore() {
        return cursoCore;
    }

    public void setCursoCore(CursoCore cursoCore) {
        this.cursoCore = cursoCore;
    }

    public Profesor getProfesor() {
        return profesor;
    }

    public void setProfesor(Profesor profesor) {
        this.profesor = profesor;
    }
}
