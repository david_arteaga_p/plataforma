package com.lp2.plataforma.horario.profesor;

import com.lp2.plataforma.curso.CursoCore;
import com.lp2.plataforma.curso.CursoService;
import com.lp2.plataforma.profesor.Profesor;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.DayOfWeek;

/**
 * Created by david on 6/20/17.
 */
public class HorarioProfesorMapper implements RowMapper<HorarioProfesor> {

    private final Profesor profesor;
    private final CursoService cursoService;

    HorarioProfesorMapper(Profesor profesor, CursoService cursoService) {
        this.profesor = profesor;
        this.cursoService = cursoService;
    }

    @Override
    public HorarioProfesor mapRow(ResultSet rs, int rowNum) throws SQLException {
        int id = rs.getInt("idHorarioProfesor");
        DayOfWeek dia = DayOfWeek.of(rs.getInt("dia"));
        int hora = rs.getInt("hora");
        boolean disponible = rs.getBoolean("disponible");

        Integer idCursoAsignado = (Integer) rs.getObject("idCursoAsignado");
        CursoCore cursoAsignado = idCursoAsignado != null ? cursoService.getCursoCoreById(idCursoAsignado) : null;

        return new HorarioProfesor(id, profesor, cursoAsignado, dia, hora, disponible);
    }
}
