package com.lp2.plataforma.horario.util;

/**
 * Created by david on 6/13/17.
 */
public class Hora {
    private int inicio;
    private int fin;

    public int getInicio() {
        return inicio;
    }

    public void setInicio(int inicio) {
        this.inicio = inicio;
    }

    public int getFin() {
        return fin;
    }

    public void setFin(int fin) {
        this.fin = fin;
    }

    public Hora(int inicio, int fin) {

        this.inicio = inicio;
        this.fin = fin;
    }
}
