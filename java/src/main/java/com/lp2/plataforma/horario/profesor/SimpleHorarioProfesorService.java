package com.lp2.plataforma.horario.profesor;

import com.lp2.plataforma.curso.CursoService;
import com.lp2.plataforma.horario.Horario;
import com.lp2.plataforma.profesor.Profesor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by david on 6/20/17.
 */
@Service
public class SimpleHorarioProfesorService implements HorarioProfesorService {

    private static final Logger logger = LoggerFactory.getLogger(SimpleHorarioProfesorService.class);

    private final JdbcTemplate jdbcTemplate;
    private final CursoService cursoService;

    @Autowired
    public SimpleHorarioProfesorService(JdbcTemplate jdbcTemplate, CursoService cursoService) {
        this.jdbcTemplate = jdbcTemplate;
        this.cursoService = cursoService;
    }

    @Override
    public List<HorarioProfesor> getHorariosForProfesor(Profesor profesor) {
        HorarioProfesorMapper  horarioProfesorMapper = new HorarioProfesorMapper(profesor, cursoService);
        try {
            return jdbcTemplate.query("select * from HorarioProfesor where idProfesor = ?", horarioProfesorMapper, profesor.getId());
        } catch (Exception e) {
            logger.info("el profesor con id " + profesor.getId() + " no tiene horarios de asesoría");
            return null;
        }
    }

    @Override
    public void updateHorariosProfesor(Profesor profesor, List<Horario> horarios) {
        Set<Horario> nuevosHorarios = new HashSet<>(horarios);

        // todos los horarios que fueron de-seleccionados deben ser eliminados
        Set<Horario> horariosEliminar = new HashSet<>(profesor.getHorarios());
        horariosEliminar.removeAll(nuevosHorarios);
        horariosEliminar = horariosEliminar.stream()
                .filter(Horario::isDisponible) // para no eliminar los no disponibles; estos están deshabilitados en el html,
                .collect(Collectors.toSet());  // por lo que no se envían como parte de los nuevos horarios

        horariosEliminar.forEach(h -> eliminarHorario(profesor, h));

        // guardar nuevos horarios, los nuevos que no estaban en la base de datos
        nuevosHorarios.removeAll(new HashSet<>(profesor.getHorarios()));
        nuevosHorarios.forEach(h -> agregarHorario(profesor, h));
    }

    private void agregarHorario(Profesor profesor, Horario horario) {
        //TODO
        jdbcTemplate.update("insert into HorarioProfesor (idProfesor, dia, hora) values (?, ?, ?)", profesor.getId(), horario.getDia().getValue(), horario.getHora());
    }

    private void eliminarHorario(Profesor profesor, Horario horario) {
        //TODO
        jdbcTemplate.update("delete from HorarioProfesor where idProfesor = ? and dia = ? and hora = ?",
                profesor.getId(), horario.getDia().getValue(), horario.getHora());
    }
}
