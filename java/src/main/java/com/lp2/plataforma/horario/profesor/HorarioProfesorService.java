package com.lp2.plataforma.horario.profesor;

import com.lp2.plataforma.horario.Horario;
import com.lp2.plataforma.profesor.Profesor;

import java.util.List;

/**
 * Created by david on 6/20/17.
 */
public interface HorarioProfesorService {
    List<HorarioProfesor> getHorariosForProfesor(Profesor profesor);
    void updateHorariosProfesor(Profesor profesor, List<Horario> horarios);
}
