package com.lp2.plataforma.horario.curso;

import com.lp2.plataforma.curso.CursoCore;
import com.lp2.plataforma.horario.Horario;
import com.lp2.plataforma.profesor.Profesor;

import java.util.List;
import java.util.Map;

/**
 * Created by david on 6/22/17.
 */
public interface HorarioCursoService {
    List<HorarioCurso> getHorariosForCurso(CursoCore cursoCore);
    boolean agregarHorarioCursoProfesor(CursoCore cursoCore, Profesor profesor, Horario horario);
    boolean eliminarHorarioCursoProfesor(CursoCore cursoCore, Profesor profesor, Horario horario);
}
