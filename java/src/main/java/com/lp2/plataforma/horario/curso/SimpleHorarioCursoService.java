package com.lp2.plataforma.horario.curso;

import com.lp2.plataforma.curso.CursoCore;
import com.lp2.plataforma.horario.Horario;
import com.lp2.plataforma.profesor.Profesor;
import com.lp2.plataforma.profesor.ProfesorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by david on 6/22/17.
 */
@Service
public class SimpleHorarioCursoService implements HorarioCursoService {

    private final JdbcTemplate jdbcTemplate;
    private final ProfesorService profesorService;

    @Autowired
    public SimpleHorarioCursoService(JdbcTemplate jdbcTemplate, ProfesorService profesorService) {
        this.jdbcTemplate = jdbcTemplate;
        this.profesorService = profesorService;
    }

    @Override
    public List<HorarioCurso> getHorariosForCurso(CursoCore cursoCore) {
        HorarioCursoMapper horarioCursoMapper = new HorarioCursoMapper(cursoCore, profesorService);
        return jdbcTemplate.query("select * from HorarioCurso where idCurso = ?", horarioCursoMapper, cursoCore.getId());
    }

    @Override
    @Transactional
    public boolean agregarHorarioCursoProfesor(CursoCore cursoCore, Profesor profesor, Horario horario) {
        int countChanged = jdbcTemplate.update("update HorarioProfesor set disponible = false, idCursoAsignado = ? where idProfesor = ? and dia = ? and hora = ?",
                cursoCore.getId(), profesor.getId(), horario.getDia().getValue(), horario.getHora());
        return countChanged == 1
                && 1 == jdbcTemplate.update("insert into HorarioCurso (idCurso, idProfesor, dia, hora) values (?, ?, ?, ?)",
                cursoCore.getId(), profesor.getId(), horario.getDia().getValue(), horario.getHora());
    }

    @Override
    @Transactional
    public boolean eliminarHorarioCursoProfesor(CursoCore cursoCore, Profesor profesor, Horario horario) {
        int countChanged = jdbcTemplate.update("update HorarioProfesor set disponible = true, idCursoAsignado = null where idProfesor = ? and dia = ? and hora = ?",
                profesor.getId(), horario.getDia().getValue(), horario.getHora());
        return countChanged == 1
                && 1 == jdbcTemplate.update("delete from HorarioCurso where idCurso = ? and idProfesor = ? and dia = ? and hora = ?", cursoCore.getId(), profesor.getId(), horario.getDia().getValue(), horario.getHora());
    }
}
