package com.lp2.plataforma.horario.profesor;

import com.lp2.plataforma.curso.CursoCore;
import com.lp2.plataforma.horario.Horario;
import com.lp2.plataforma.profesor.Profesor;

import java.time.DayOfWeek;

/**
 * Created by david on 6/20/17.
 */
public class HorarioProfesor extends Horario {
    private int id;
    private Profesor profesor;
    private CursoCore cursoAsignado;

    public HorarioProfesor(int id, Profesor profesor, CursoCore cursoAsignado, DayOfWeek dia, int hora, boolean disponible) {
        super(dia, hora, disponible);
        this.id = id;
        this.profesor = profesor;
        this.cursoAsignado = cursoAsignado;
    }

    public int getId() {

        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Profesor getProfesor() {
        return profesor;
    }

    public void setProfesor(Profesor profesor) {
        this.profesor = profesor;
    }

    public CursoCore getCursoAsignado() {
        return cursoAsignado;
    }

    public void setCursoAsignado(CursoCore cursoAsignado) {
        this.cursoAsignado = cursoAsignado;
    }

    @Override
    public String toString() {
        return "HorarioProfesor{" +
                "id=" + id +
                ", profesor=" + profesor.getId() +
                ", cursoAsignado=" + cursoAsignado +
                "} " + super.toString();
    }
}
