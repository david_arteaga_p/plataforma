package com.lp2.plataforma.horario.util;

/**
 * Created by david on 6/13/17.
 */
public class Dia {
    private String nombre;
    private int num;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public Dia(String nombre, int num) {

        this.nombre = nombre;
        this.num = num;
    }
}
