package com.lp2.plataforma.horario;

import com.lp2.plataforma.horario.profesor.HorarioProfesor;

import java.time.DayOfWeek;

/**
 * Created by david on 6/20/17.
 */
public class Horario {
    private DayOfWeek dia;
    private int hora;
    private boolean disponible;

    public String getStringId() {
        return String.format("slot_%d_%d", dia.getValue(), hora);
    }

    public Horario(DayOfWeek dia, int hora, boolean disponible) {
        this.dia = dia;
        this.hora = hora;
        this.disponible = disponible;
    }

    public DayOfWeek getDia() {
        return dia;
    }

    public void setDia(DayOfWeek dia) {
        this.dia = dia;
    }

    public int getHora() {
        return hora;
    }

    public void setHora(int hora) {
        this.hora = hora;
    }

    public boolean isDisponible() {
        return disponible;
    }

    public void setDisponible(boolean disponible) {
        this.disponible = disponible;
    }

    @Override
    public String toString() {
        return "Horario{" +
                "dia=" + dia +
                ", hora=" + hora +
                ", disponible=" + disponible +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Horario)) return false;

        Horario horario = (Horario) o;

        return getHora() == horario.getHora() && getDia() == horario.getDia();
    }

    @Override
    public int hashCode() {
        int result = getDia().hashCode();
        result = 31 * result + getHora();
        return result;
    }

}
