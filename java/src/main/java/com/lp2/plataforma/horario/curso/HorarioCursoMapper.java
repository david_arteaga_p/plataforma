package com.lp2.plataforma.horario.curso;

import com.lp2.plataforma.curso.CursoCore;
import com.lp2.plataforma.profesor.Profesor;
import com.lp2.plataforma.profesor.ProfesorService;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.DayOfWeek;
import java.util.Map;

/**
 * Created by david on 6/22/17.
 */
public class HorarioCursoMapper implements RowMapper<HorarioCurso> {

    private final CursoCore cursoCore;
    private final ProfesorService profesorService;

    public HorarioCursoMapper(CursoCore cursoCore, ProfesorService profesorService) {
        this.cursoCore = cursoCore;
        this.profesorService = profesorService;
    }

    @Override
    public HorarioCurso mapRow(ResultSet rs, int rowNum) throws SQLException {
        int id = rs.getInt("idHorarioCurso");
        int idProfesor = rs.getInt("idProfesor");
        Profesor profesor = profesorService.getProfesorById(idProfesor);
        DayOfWeek dia = DayOfWeek.of(rs.getInt("dia"));
        int hora = rs.getInt("hora");
        boolean disponible = rs.getBoolean("disponible");

        return new HorarioCurso(id, cursoCore, profesor, dia, hora, disponible);
    }
}
