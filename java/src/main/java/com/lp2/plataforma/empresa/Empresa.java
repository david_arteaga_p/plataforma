package com.lp2.plataforma.empresa;

import com.lp2.config.security.user.User;
import com.lp2.config.security.user.UserBean;
import com.lp2.web.form.EmpresaForm;

/**
 * Created by david on 4/7/17.
 */
public class Empresa extends UserBean {
    private String razonSocial;
    private Long ruc;
    private String rubro;
    private Long telefono;
    private String email;
    private String descripcion;

    Empresa(User user, String razonSocial, Long ruc, String rubro, Long telefono, String email, String descripcion) {
        super(user);
        this.razonSocial = razonSocial;
        this.ruc = ruc;
        this.rubro = rubro;
        this.telefono = telefono;
        this.email = email;
        this.descripcion = descripcion;
    }

    Empresa(User user, EmpresaForm empresaForm) {
        super(user);
        this.razonSocial = empresaForm.getRazonSocial();
        this.ruc = empresaForm.getRuc();
        this.rubro = empresaForm.getRubro();
        this.telefono = empresaForm.getTelefono();
        this.email = empresaForm.getEmail();
        this.descripcion = empresaForm.getDescripcion();
    }

    @Override
    public String toString() {
        return super.toString() + "::\n::Empresa{" +
                "razonSocial='" + razonSocial + '\'' +
                ", ruc=" + ruc +
                ", rubro='" + rubro + '\'' +
                ", telefono=" + telefono +
                ", email='" + email + '\'' +
                ", descripcion='" + descripcion + '\'' +
                '}';
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public Long getRuc() {
        return ruc;
    }

    public void setRuc(Long ruc) {
        this.ruc = ruc;
    }

    public String getRubro() {
        return rubro;
    }

    public void setRubro(String rubro) {
        this.rubro = rubro;
    }

    public Long getTelefono() {
        return telefono;
    }

    public void setTelefono(Long telefono) {
        this.telefono = telefono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
