package com.lp2.plataforma.empresa;


import com.lp2.config.security.user.User;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by david on 6/10/17.
 */
public class EmpresaMapper implements RowMapper<Empresa> {

    private final User user;

    public EmpresaMapper(User user) {
        this.user = user;
    }

    @Override
    public Empresa mapRow(ResultSet rs, int rowNum) throws SQLException {
        return new Empresa(
                user,
                rs.getString("razonSocial"),
                (Long) rs.getObject("ruc"),
                rs.getString("rubro"),
                (Long) rs.getObject("telefono"),
                rs.getString("email"),
                rs.getString("descripcion"));
    }

}
