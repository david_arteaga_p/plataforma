package com.lp2.plataforma.empresa;

import com.lp2.config.security.user.NewUser;
import com.lp2.config.security.user.User;
import com.lp2.config.security.user.UserBean;
import com.lp2.config.security.user.UserService;
import com.lp2.web.form.EmpresaForm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.UUID;

/**
 * Created by david on 6/10/17.
 */
@Service
public class SimpleEmpresaService implements EmpresaService {

    private static final Logger logger = LoggerFactory.getLogger(SimpleEmpresaService.class);

    private final JdbcTemplate jdbcTemplate;
    private final UserService userService;

    @Autowired
    public SimpleEmpresaService(JdbcTemplate jdbcTemplate, UserService userService) {
        this.jdbcTemplate = jdbcTemplate;
        this.userService = userService;
    }

    @Override
    public Empresa createEmpresa(EmpresaForm empresaForm) {
        UserBean user = userService.createUser(new NewUser() {
            @Override
            public String getUsuario() {
                return empresaForm.getUsuario();
            }

            @Override
            public String getPassword() {
                return empresaForm.getPassword();
            }

            @Override
            public boolean isEnabled() {
                return true;
            }
        }, null);

        // crear empresa con mismo idEmpresa que idUsuario
        jdbcTemplate.update("insert into Empresa (razonSocial, ruc, Usuario_idUsuario, rubro, telefono, email, descripcion) values (?, ?, ?, ?, ?, ?, ?);",
                empresaForm.getRazonSocial(),
                empresaForm.getRuc(),
                user.getId(),
                empresaForm.getRubro(),
                empresaForm.getTelefono(),
                empresaForm.getEmail(),
                empresaForm.getDescripcion());

        // set id empresa del usuario recién creado
        jdbcTemplate.update("update Usuario set idEmpresa = ? where idUsuario = ?", user.getId(), user.getId());

        Empresa empresa = new Empresa(user, empresaForm);

        user.setEmpresa(empresa);

        return empresa;
    }

    @Override
    public Empresa getEmpresaByUsername(String username) {
        User user = userService.getUserByUsername(username);
        if (user == null) {
            throw new RuntimeException("Usuario de empresa no existente: " + username);
        }
        return user.getEmpresa();
    }

    @Override
    public Empresa getEmpresaById(int idEmpresa) {
        User user = userService.getUserById(idEmpresa);

        return user != null ? user.getEmpresa() : null;
    }

    @Override
    @Transactional
    public boolean editarEmpresa(Empresa empresa, EmpresaForm empresaForm) {
        int count = jdbcTemplate.update("update Empresa set razonSocial = ?, rubro = ?, telefono = ?, email = ?, descripcion = ? where Usuario_idUsuario = ?",
                empresaForm.getRazonSocial(), empresaForm.getRubro(), empresaForm.getTelefono(), empresaForm.getEmail(), empresaForm.getDescripcion(),
                empresa.getId());

        boolean password;
        if (empresaForm.getPassword() != null && !empresaForm.getPassword().isEmpty()) {
            password = userService.changePassword(empresa, empresaForm.getPassword());
        } else {
            password = true;
        }

        return count == 1 && password;
    }

    @Override
    public Empresa getEmpresaByEmail(String email) {
        try {
            int id = jdbcTemplate.queryForObject("select Usuario_idUsuario from Empresa where email = ?", Integer.class, email.replace(" ", ""));
            return userService.getUserById(id).getEmpresa();
        } catch (Exception e) {
            logger.info("No existe empresa con email " + email);
            return null;
        }
    }

    @Override
    public Empresa getEmpresaForEmpresaUserId(int idEmpresa, User empresaUser) {
        assert empresaUser != null && idEmpresa == empresaUser.getId();

        EmpresaMapper empresaMapper = new EmpresaMapper(empresaUser);

        return jdbcTemplate.queryForObject("select * from Empresa where Usuario_idUsuario = ?", empresaMapper, idEmpresa);
    }

}
