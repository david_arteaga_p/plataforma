package com.lp2.plataforma.empresa;

import com.lp2.config.security.user.User;
import com.lp2.web.form.EmpresaForm;

import java.security.Principal;

/**
 * Created by david on 6/10/17.
 */
public interface EmpresaService {
    User createEmpresa(EmpresaForm empresaForm);
    Empresa getEmpresaByUsername(String username);
    Empresa getEmpresaForEmpresaUserId(int idEmpresa, User empresaUser);
    Empresa getEmpresaById(int idEmpresa);
    boolean editarEmpresa(Empresa empresa, EmpresaForm empresaForm);

    public Empresa getEmpresaByEmail(String email);
}
