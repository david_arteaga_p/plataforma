package com.lp2.plataforma;

import com.lp2.plataforma.alumno.Alumno;
import com.lp2.plataforma.curso.CursoCore;

import java.time.LocalDateTime;
import java.util.Currency;

/**
 * Created by david on 4/8/17.
 */
public class Pago {
    private int id;
    private double monto;
    private LocalDateTime fecha;
    private Currency tipoDeMoneda;
    private CursoCore cursoCore;
    private Alumno alumno;

    public Pago() {
    }

    public Pago(int id, double monto, LocalDateTime fecha, Currency tipoDeMoneda, CursoCore cursoCore, Alumno alumno) {
        this.id = id;
        this.monto = monto;
        this.fecha = fecha;
        this.tipoDeMoneda = tipoDeMoneda;
        this.cursoCore = cursoCore;
        this.alumno = alumno;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getMonto() {
        return monto;
    }

    public void setMonto(double monto) {
        this.monto = monto;
    }

    public LocalDateTime getFecha() {
        return fecha;
    }

    public void setFecha(LocalDateTime fecha) {
        this.fecha = fecha;
    }

    public Currency getTipoDeMoneda() {
        return tipoDeMoneda;
    }

    public void setTipoDeMoneda(Currency tipoDeMoneda) {
        this.tipoDeMoneda = tipoDeMoneda;
    }

    public CursoCore getCursoCore() {
        return cursoCore;
    }

    public void setCursoCore(CursoCore cursoCore) {
        this.cursoCore = cursoCore;
    }

    public Alumno getAlumno() {
        return alumno;
    }

    public void setAlumno(Alumno alumno) {
        this.alumno = alumno;
    }
}
