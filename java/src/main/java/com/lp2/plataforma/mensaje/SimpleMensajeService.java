package com.lp2.plataforma.mensaje;

import com.lp2.plataforma.empresa.Empresa;
import com.lp2.plataforma.pregunta.Pregunta;
import com.lp2.web.form.MensajeForm;
import com.mysql.jdbc.Statement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.time.LocalDate;
import java.util.List;

import static com.lp2.model.SqlHelper.setDouble;
import static com.lp2.model.SqlHelper.setInteger;

/**
 * Created by david on 5/9/17.
 */
@Service
public class SimpleMensajeService implements MensajeService {

    private final JdbcTemplate jdbcTemplate;
    private final MensajeMapper mensajeMapper;

    @Autowired
    public SimpleMensajeService(JdbcTemplate jdbcTemplate, MensajeMapper mensajeMapper) {
        this.jdbcTemplate = jdbcTemplate;
        this.mensajeMapper = mensajeMapper;
    }

    @Override
    public List<Mensaje> getMensajesForPregunta(Pregunta pregunta) {
        return jdbcTemplate.query("select * from Mensajes where Preguntas_idPreguntas = ?;", mensajeMapper, pregunta.getId());
    }

    /*
    * idMensajes
Contenido
Preguntas_idPreguntas
Preguntas_Curso_idCurso
remitente_idUsuario
destinatario_idUsuario
leido
    * */

    @Override
    public Mensaje agregarMensajePregunta(MensajeForm mensajeForm, Pregunta pregunta, Empresa empresa) {
        try {
            String sql = "insert into Mensajes (Contenido, Preguntas_idPreguntas, Preguntas_Curso_idCurso, remitente_idUsuario, destinatario_idUsuario)" +
                    " values (?, ?, ?, ?, ?);";

            KeyHolder keyHolder = new GeneratedKeyHolder();

            jdbcTemplate.update(con -> {
                PreparedStatement statement = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                statement.setString(1, mensajeForm.getContenido());
                statement.setInt(2, pregunta.getId());
                statement.setInt(3, pregunta.getCursoCore().getId());
                statement.setInt(4, empresa.getId());
                statement.setInt(5, pregunta.getAlumno().getId());
                return statement;
            }, keyHolder);

            return new Mensaje(keyHolder.getKey().intValue(), mensajeForm, pregunta, empresa);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void setRead(Mensaje mensaje) {
        // TODO
        jdbcTemplate.update("update Mensajes set leido = true where idMensajes = ?", mensaje.getId());
    }
}
