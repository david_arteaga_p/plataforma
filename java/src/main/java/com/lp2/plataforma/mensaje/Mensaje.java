package com.lp2.plataforma.mensaje;

import com.lp2.config.security.user.User;
import com.lp2.plataforma.empresa.Empresa;
import com.lp2.plataforma.pregunta.Pregunta;
import com.lp2.web.form.MensajeForm;

/**
 * Created by david on 4/8/17.
 */
public class Mensaje {
    private int id;
    private String contenido;
    private User remitente;
    private User destinatario;
    private boolean leido;

    Mensaje(int id, String contenido, User remitente, User destinatario, boolean leido) {
        this.id = id;
        this.contenido = contenido;
        this.remitente = remitente;
        this.destinatario = destinatario;
        this.leido = leido;
    }

    Mensaje(int id, MensajeForm mensajeForm, Pregunta pregunta, Empresa empresa) {
        this.id = id;
        this.contenido = mensajeForm.getContenido();
        this.remitente = empresa;
        this.destinatario = pregunta.getAlumno();
        leido = false;
    }

    @Override
    public String toString() {
        return "Mensaje{" +
                "id=" + id +
                ", contenido='" + contenido + '\'' +
                ", remitente=" + remitente +
                ", destinatario=" + destinatario +
                ", leido=" + leido +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContenido() {
        return contenido;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }

    public User getRemitente() {
        return remitente;
    }

    public void setRemitente(User remitente) {
        this.remitente = remitente;
    }

    public User getDestinatario() {
        return destinatario;
    }

    public void setDestinatario(User destinatario) {
        this.destinatario = destinatario;
    }

    public boolean isLeido() {
        return leido;
    }

    public void setLeido(boolean leido) {
        this.leido = leido;
    }

    
}
