package com.lp2.plataforma.mensaje;

import com.lp2.plataforma.empresa.Empresa;
import com.lp2.plataforma.pregunta.Pregunta;
import com.lp2.web.form.MensajeForm;

import java.util.List;

/**
 * Created by david on 5/9/17.
 */
public interface MensajeService {
    List<Mensaje> getMensajesForPregunta(Pregunta pregunta);
    Mensaje agregarMensajePregunta(MensajeForm mensajeForm, Pregunta pregunta, Empresa empresa);
    void setRead(Mensaje mensaje);
}
