package com.lp2.plataforma.mensaje;

import com.lp2.config.security.user.User;
import com.lp2.config.security.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by david on 5/9/17.
 */
@Component
public class MensajeMapper implements RowMapper<Mensaje> {

    private final UserService userService;

    @Autowired
    public MensajeMapper(UserService userService) {
        this.userService = userService;
    }

    /**
     * Implementations must implement this method to map each row of data
     * in the ResultSet. This method should not call {@code next()} on
     * the ResultSet; it is only supposed to map values of the current row.
     *
     * @param rs     the ResultSet to map (pre-initialized for the current row)
     * @param rowNum the number of the current row
     * @return the result object for the current row
     * @throws SQLException if a SQLException is encountered getting
     *                      column values (that is, there's no need to catch SQLException)
     */
    @Override
    public Mensaje mapRow(ResultSet rs, int rowNum) throws SQLException {

        int id = rs.getInt("idMensajes");

        String contenido = rs.getString("Contenido");

        int idRemitente = rs.getInt("remitente_idUsuario");
        int idDestinatario = rs.getInt("destinatario_idUsuario");

        User remitente = userService.getUserById(idRemitente);
        User destinatario = userService.getUserById(idDestinatario);

        boolean leido = rs.getBoolean("leido");


        return new Mensaje(id, contenido, remitente, destinatario, leido);
    }
}
