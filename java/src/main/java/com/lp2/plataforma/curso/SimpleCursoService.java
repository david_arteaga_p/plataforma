package com.lp2.plataforma.curso;

import static com.lp2.model.SqlHelper.*;

import com.lp2.plataforma.empresa.Empresa;
import com.lp2.plataforma.empresa.EmpresaService;
import com.lp2.plataforma.profesor.Profesor;
import com.lp2.web.form.CursoForm;
import com.mysql.jdbc.Statement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Created by david on 5/9/17.
 */
@Service
public class SimpleCursoService implements CursoService {

    private static final Logger logger = LoggerFactory.getLogger(SimpleCursoService.class);

    private final JdbcTemplate jdbcTemplate;
    private final EmpresaService empresaService;

    @Autowired
    public SimpleCursoService(JdbcTemplate jdbcTemplate, EmpresaService empresaService) {
        this.jdbcTemplate = jdbcTemplate;
        this.empresaService = empresaService;
    }

    @Override
    public CursoCore createCurso(CursoForm cursoForm, Empresa empresa) {
        String sql = "insert into Curso (nombre, costo, horasDuracion, fechaInicio, fechaFin, sumilla, activo, Empresa_Usuario_idUsuario, vacantes)" +
                " values (?, ?, ?, ?, ?, ?, ?, ?, ?);";

        KeyHolder keyHolder = new GeneratedKeyHolder();

        LocalDate inicio = cursoForm.getFechaInicio();
        LocalDate fin = cursoForm.getFechaFin();

        jdbcTemplate.update(con -> {
            PreparedStatement statement = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, cursoForm.getNombre());
            setDouble(statement, 2, cursoForm.getCosto());
            setInteger(statement, 3, cursoForm.getHorasDuracion());
            statement.setDate(4, inicio != null ? Date.valueOf(inicio) : null);
            statement.setDate(5, fin != null ? Date.valueOf(fin) : null);
            statement.setString(6, cursoForm.getSumilla());
            statement.setBoolean(7, false);
            statement.setInt(8, empresa.getId());
            setInteger(statement, 9, cursoForm.getVacantes());
            return statement;
        }, keyHolder);

        return new CursoCore(keyHolder.getKey().intValue(), cursoForm.getNombre(), cursoForm.getSumilla(), cursoForm.getFechaInicio(), cursoForm.getFechaFin(),
                cursoForm.getHorasDuracion(), cursoForm.getVacantes(), cursoForm.getCosto(), false, empresa);
    }

    @Override
    public List<CursoCore> getCursosForEmpresa(Empresa empresa) {
        List<Integer> cursosIds = jdbcTemplate.query("select idCurso from Curso where Empresa_Usuario_idUsuario = ?;", (rs, i) -> rs.getInt("idCurso"), empresa.getId());
        return cursosIds.stream()
                .map(id -> getCursoByIdForEmpresa(id, empresa))
                .collect(Collectors.toList());
    }

    @Override
    public CursoCore getCursoCoreById(int id) {
        try {
            int idEmpresa = jdbcTemplate.queryForObject("select Empresa_Usuario_idUsuario from Curso where idCurso = ?;", (rs, i) -> rs.getInt("Empresa_Usuario_idUsuario"), id);
            Empresa empresa = empresaService.getEmpresaById(idEmpresa);
            return this.getCursoByIdForEmpresa(id, empresa);
        } catch (DataAccessException ex) {
            return null;
        }
    }

    private CursoCore getCursoByIdForEmpresa(int id, Empresa empresa) {
        CursoCoreMapper cursoCoreMapper = new CursoCoreMapper(empresa);
        try {
            return jdbcTemplate.queryForObject("select * from Curso where idCurso = ?;", cursoCoreMapper, id);
        } catch (DataAccessException ex) {
            return null;
        }
    }

    @Override
    public void editarCurso(CursoForm cursoForm) {
        String sql = "update Curso set nombre = ?, costo = ?, horasDuracion = ?, fechaInicio = ?, fechaFin = ?, sumilla = ?, activo = ?, vacantes = ? where idCurso = ?;";

        jdbcTemplate.update(con -> {
            PreparedStatement statement = con.prepareStatement(sql);
            statement.setString(1, cursoForm.getNombre());
            setDouble(statement, 2, cursoForm.getCosto());
            setInteger(statement, 3, cursoForm.getHorasDuracion());
            setDate(statement, 4, cursoForm.getFechaInicio());
            setDate(statement, 5, cursoForm.getFechaFin());
            statement.setString(6, cursoForm.getSumilla());
            statement.setBoolean(7, cursoForm.isActivo());
            setInteger(statement, 8, cursoForm.getVacantes());
            statement.setInt(9, cursoForm.getId());
            return statement;
        });
    }

    @Override
    public boolean agregarProfesorCurso(Profesor profesor, CursoCore cursoCore) {
        try {
            int count = jdbcTemplate.update("insert into Curso_has_Profesor (Curso_idCurso, Profesor_idProfesor) values (?, ?)", cursoCore.getId(), profesor.getId());
            return count == 1;
        } catch (DataAccessException e) {
            return false;
        }
    }

    @Override
    @Transactional
    public boolean eliminarProfesorCurso(Profesor profesor, CursoCore cursoCore) {
        int count = jdbcTemplate.update("delete from Curso_has_Profesor where Curso_idCurso = ? and Profesor_idProfesor = ?", cursoCore.getId(), profesor.getId());
        long countHorariosProfesor = profesor.getHorarios().stream()
                .filter(horario -> !horario.isDisponible() && Objects.equals(horario.getCursoAsignado().getId(), cursoCore.getId()))
                .count();
        boolean deletedAll = countHorariosProfesor == jdbcTemplate.update("delete from HorarioCurso where idCurso = ? and idProfesor = ?",
                cursoCore.getId(), profesor.getId());
        boolean freedAll = countHorariosProfesor ==
                jdbcTemplate.update("update HorarioProfesor set disponible = true, idCursoAsignado = null where idProfesor = ? and idCursoAsignado = ?",
                        profesor.getId(), cursoCore.getId());
        return count == 1 && deletedAll && freedAll;
    }

    @Override
    public boolean guardarImagen(CursoCore cursoCore, MultipartFile imagenCurso) {

        try {
            int count = jdbcTemplate.update("insert into ImagenCurso (idCurso, imageData) values (?, ?)", cursoCore.getId(), imagenCurso.getBytes());
            return count == 1;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public byte[] getImagenForCurso(CursoCore cursoCore) {
        try {
            return jdbcTemplate.queryForObject("select imageData from ImagenCurso where idCurso = ?", byte[].class, cursoCore.getId());
        } catch (Exception e) {
            logger.info("no image for curso with id " + cursoCore.getId());
            return null;
        }
    }
}
