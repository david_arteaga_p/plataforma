package com.lp2.plataforma.curso;

import com.lp2.model.SqlHelper;
import com.lp2.plataforma.empresa.Empresa;
import com.lp2.plataforma.empresa.EmpresaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;

/**
 * Created by david on 5/9/17.
 */
public class CursoCoreMapper implements RowMapper<CursoCore> {

    private final Empresa empresa;

    CursoCoreMapper(Empresa empresa) {
        this.empresa = empresa;
    }

    /**
     * Implementations must implement this method to map each row of data
     * in the ResultSet. This method should not call {@code next()} on
     * the ResultSet; it is only supposed to map values of the current row.
     *
     * @param rs     the ResultSet to map (pre-initialized for the current row)
     * @param rowNum the number of the current row
     * @return the result object for the current row
     * @throws SQLException if a SQLException is encountered getting
     *                      column values (that is, there's no need to catch SQLException)
     */
    @Override
    public CursoCore mapRow(ResultSet rs, int rowNum) throws SQLException {

        int id = rs.getInt("idCurso");
        String nombre = rs.getString("nombre");
        String sumilla = rs.getString("sumilla");
        LocalDate fechaInicio = SqlHelper.getLocalDate(rs, "fechaInicio");
        LocalDate fechaFin = SqlHelper.getLocalDate(rs, "fechaFin");
        Integer horasDuracion = (Integer) rs.getObject("horasDuracion");
        Integer vacantes = (Integer) rs.getObject("vacantes");
        Double costo = (Double) rs.getObject("costo");
        boolean activo = rs.getBoolean("activo");

        assert empresa.getId() == rs.getInt("Empresa_Usuario_idUsuario");

        return new CursoCore(id, nombre, sumilla, fechaInicio, fechaFin, horasDuracion, vacantes, costo, activo, empresa);
    }

}
