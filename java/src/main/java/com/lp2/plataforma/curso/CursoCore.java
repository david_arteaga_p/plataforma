package com.lp2.plataforma.curso;

import com.lp2.plataforma.empresa.Empresa;

import java.time.LocalDate;

/**
 * Created by david on 4/7/17.
 */
public class CursoCore {
    private Integer id;
    private String nombre;
    private String sumilla;
    private LocalDate fechaInicio;
    private LocalDate fechaFin;
    private Integer horasDuracion;
    private Integer vacantes;
    private Double costo;
    private boolean activo;
    private Empresa empresa;

    public CursoCore() {}

    CursoCore(Integer id, String nombre, String sumilla, LocalDate fechaInicio, LocalDate fechaFin, Integer horasDuracion, Integer vacantes, Double costo, boolean activo, Empresa empresa) {
        this.id = id;
        this.nombre = nombre;
        this.sumilla = sumilla;
        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
        this.horasDuracion = horasDuracion;
        this.vacantes = vacantes;
        this.costo = costo;
        this.activo = activo;
        this.empresa = empresa;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Double getCosto() {
        return costo;
    }

    public void setCosto(Double costo) {
        this.costo = costo;
    }

    public Integer getHorasDuracion() {
        return horasDuracion;
    }

    public void setHorasDuracion(Integer horasDuracion) {
        this.horasDuracion = horasDuracion;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    public LocalDate getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(LocalDate fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public LocalDate getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(LocalDate fechaFin) {
        this.fechaFin = fechaFin;
    }

    public String getSumilla() {
        return sumilla;
    }

    public void setSumilla(String sumilla) {
        this.sumilla = sumilla;
    }

    public Integer getVacantes() {
        return vacantes;
    }

    public void setVacantes(Integer vacantes) {
        this.vacantes = vacantes;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    @Override
    public String toString() {
        return "CursoCore{" +
                "id=" + id +
                ", nombre='" + nombre + '\'' +
                ", sumilla='" + sumilla + '\'' +
                ", fechaInicio=" + fechaInicio +
                ", fechaFin=" + fechaFin +
                ", horasDuracion=" + horasDuracion +
                ", vacantes=" + vacantes +
                ", costo=" + costo +
                ", activo=" + activo +
                ", empresa=" + empresa +
                '}';
    }
}
