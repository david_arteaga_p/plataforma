package com.lp2.plataforma.curso;

import com.lp2.plataforma.alumno.Alumno;
import com.lp2.plataforma.empresa.Empresa;
import com.lp2.plataforma.horario.Horario;
import com.lp2.plataforma.horario.curso.HorarioCurso;
import com.lp2.plataforma.pregunta.Pregunta;
import com.lp2.plataforma.profesor.Profesor;

import java.util.Comparator;
import java.util.List;

/**
 * Created by david on 6/10/17.
 */
public class Curso extends CursoCore {
    private List<Profesor> profesores;
    private List<Pregunta> preguntas;
    private List<Alumno> alumnos;
    private Empresa empresa;

    public Curso(CursoCore cursoCore,
                 List<Profesor> profesores, List<Pregunta> preguntas, List<Alumno> alumnos, Empresa empresa) {
        super(cursoCore.getId(), cursoCore.getNombre(), cursoCore.getSumilla(), cursoCore.getFechaInicio(), cursoCore.getFechaFin(),
                cursoCore.getHorasDuracion(), cursoCore.getVacantes(), cursoCore.getCosto(), cursoCore.isActivo(), empresa);
        this.profesores = profesores;
        this.preguntas = preguntas;
        if (preguntas != null ) {
            this.preguntas.sort(PREGUNTA_COMPARATOR_MAS_NO_LEIDOS);
        }
        this.alumnos = alumnos;
        this.empresa = empresa;
    }

    private static final Comparator<Pregunta> PREGUNTA_COMPARATOR_MAS_NO_LEIDOS = (left, right) -> {
        if (right.getCantidadMensajesNoLeidos() < left.getCantidadMensajesNoLeidos()) {
            return -1;
        } else if (left.getCantidadMensajesNoLeidos() < right.getCantidadMensajesNoLeidos()) {
            return 1;
        } else {
            if (left.getId() < right.getId()) {
                return -1;
            }
            return 1;
        }
    };

    public Curso() {
        super();
    }

    // para mostrar solo n en la página admin del curso
    public List<Profesor> profesores(int count) {
        return profesores.subList(0, Math.min(count, profesores.size()));
    }

    // para mostrar solo n en la página admin del curso
    public List<Pregunta> preguntas(int count) {
        return preguntas.subList(0, Math.min(count, preguntas.size()));
    }

    // para mostrar solo n
    public List<Alumno> alumnos(int count) {
        return alumnos.subList(0, Math.min(count, alumnos.size()));
    }

    public List<Profesor> getProfesores() {
        return profesores;
    }

    public void setProfesores(List<Profesor> profesores) {
        this.profesores = profesores;
    }

    public List<Pregunta> getPreguntas() {
        return preguntas;
    }

    public void setPreguntas(List<Pregunta> preguntas) {
        this.preguntas = preguntas;
    }

    public List<Alumno> getAlumnos() {
        return alumnos;
    }

    public void setAlumnos(List<Alumno> alumnos) {
        this.alumnos = alumnos;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }
}
