package com.lp2.plataforma.curso;

import com.lp2.config.security.user.User;
import com.lp2.plataforma.empresa.Empresa;
import com.lp2.plataforma.profesor.Profesor;
import com.lp2.web.form.CursoForm;
import org.springframework.web.multipart.MultipartFile;

import java.security.Principal;
import java.util.List;

/**
 * Created by david on 5/9/17.
 */
public interface CursoService {
    CursoCore createCurso(CursoForm cursoForm, Empresa empresa);
    List<CursoCore> getCursosForEmpresa(Empresa empresa);
    CursoCore getCursoCoreById(int id);
    void editarCurso(CursoForm cursoForm);
    boolean agregarProfesorCurso(Profesor profesor, CursoCore cursoCore);
    boolean eliminarProfesorCurso(Profesor profesor, CursoCore cursoCore);

    boolean guardarImagen(CursoCore cursoCore, MultipartFile imagenCurso);

    byte[] getImagenForCurso(CursoCore cursoCore);
}
