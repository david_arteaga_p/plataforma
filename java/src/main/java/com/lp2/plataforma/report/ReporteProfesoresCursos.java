package com.lp2.plataforma.report;

import com.lowagie.text.*;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.lp2.plataforma.curso.CursoCore;
import com.lp2.plataforma.horario.curso.HorarioCurso;
import com.lp2.plataforma.profesor.Profesor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.view.document.AbstractPdfView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.TextStyle;
import java.util.Locale;
import java.util.Map;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by david on 6/26/17.
 */
public class ReporteProfesoresCursos extends AbstractPdfView {

    private final Logger logger = LoggerFactory.getLogger(ReporteProfesoresCursos.class);

    private final List<CursoCore> cursos;
    private final Map<Integer, List<HorarioCurso>> horarioPorCurso;

    public ReporteProfesoresCursos(List<CursoCore> cursos, Map<Integer, List<HorarioCurso>> horarioPorCurso) {
        this.cursos = cursos;
        this.horarioPorCurso = horarioPorCurso;
    }

    @Override
    protected void buildPdfDocument(Map<String, Object> _model, Document document, PdfWriter writer, HttpServletRequest request, HttpServletResponse response) throws Exception {

        document.addTitle("Reporte de horarios de asesoría");

        document.add(title("Reporte de horarios de asesoría", 30));

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/LLLL/yyyy", Locale.forLanguageTag("es"));

        document.add(title( LocalDate.now().format(formatter), 12 ));
        document.add(Chunk.NEWLINE);

        // profesor, dia, hora inicial, hora final


        for (CursoCore curso: cursos) {
            document.add(title(curso.getNombre(), 15));
            document.add(Chunk.NEWLINE);

            List<HorarioCurso> horariosCurso = horarioPorCurso.get(curso.getId());

            Map<Profesor, List<HorarioCurso>> horariosPorProfesor = horariosCurso.stream()
                    .collect(Collectors.groupingBy(
                            horario -> horario.getProfesor(),
                            Collectors.toList()
                    ));

            PdfPTable table = new PdfPTable(1+1+1+1);

            addCell(table, "Profesor");
            addCell(table, "Día");
            addCell(table, "Hora Inicial");
            addCell(table, "Hora Final");
            table.completeRow();

            for (Map.Entry<Profesor, List<HorarioCurso>> entry: horariosPorProfesor.entrySet()) {

                Profesor profesor = entry.getKey();
                List<HorarioCurso> horariosProfesor = entry.getValue();

                addCell(table, profesor.getNombre() + ' ' + profesor.getApellido());
                table.addCell("");
                table.addCell("");
                table.addCell("");
                table.completeRow();

                for (HorarioCurso horarioCurso: horariosProfesor) {
                    table.addCell("");
                    addCell(table, horarioCurso.getDia().getDisplayName(TextStyle.FULL, Locale.forLanguageTag("es")));
                    addCell(table, horarioCurso.getHora() + "");
                    addCell(table, horarioCurso.getHora() + 1 + "");
                    table.completeRow();
                }
            }

            table.addCell("");
            table.addCell("");
            table.addCell("");
            if (horariosPorProfesor.isEmpty()) {
                document.add(new Paragraph("Este curso no tiene profesores"));
            } else {
                document.add(table);
            }

            document.add(Chunk.NEWLINE);
        }

    }

    void addCell(PdfPTable table, String content) {

        PdfPCell cell = new PdfPCell(new Phrase(content));

        //cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);

        table.addCell(cell);
    }

    private Paragraph title(String title, int size) {
        return new Paragraph(new Chunk(title, FontFactory.getFont(FontFactory.HELVETICA, size)));
    }
}
