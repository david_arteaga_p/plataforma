package com.lp2.model;

import java.sql.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by david on 3/8/17.
 */
public class SqlHelper {

    //MARK: Setting nullable column values

    public static void setInteger(PreparedStatement statement, int index, Integer value) throws SQLException {
        if (value != null)
            statement.setDouble(index, value);
        else
            statement.setNull(index, Types.INTEGER);
    }

    public static void setDouble(PreparedStatement statement, int index, Double value) throws SQLException {
        if (value != null)
            statement.setDouble(index, value);
        else
            statement.setNull(index, Types.DOUBLE);
    }

    public static void setLong(PreparedStatement statement, int index, Long value) throws SQLException {
        if (value != null)
            statement.setLong(index, value);
        else
            statement.setNull(index, Types.BIGINT);
    }

    public static void setDate(PreparedStatement statement, int index, LocalDate date) throws SQLException {
        if (date != null) {
            statement.setDate(index, Date.valueOf(date));
        } else {
            statement.setNull(index, Types.DATE);
        }
    }

    //MARK: Reading nullable values

    public static LocalDate getLocalDate(ResultSet rs, String columnName) throws SQLException {
        Date date;
        return (date = rs.getDate(columnName)) != null ? date.toLocalDate() : null;
    }

    public static LocalDateTime getLocalDateTime(ResultSet rs, String columnName) throws SQLException {
        Timestamp date;
        return (date = rs.getTimestamp(columnName)) != null ? LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault()) : null;
    }

    /**
     * Get a string of the form 'colName = val1 <delimiter> colName = val2 ...' for querying
     * a collection of objects.
     * @param values the values you wish to concatenate
     * @param columnName the name of the column the values correspond to
     * @param delimiter the string that will join each value
     * @return the querying string with the values and column name joined by the delimiter
     */
    public static String joinColumnValuesBy(List<?> values, String columnName, String delimiter) {
        return String.join(' ' + delimiter + ' ', values.stream().map(val -> columnName + " = '" + val + "'").collect(Collectors.toList()));
    }


}
