#!/bin/bash
find $1 -name "*.html" -exec sed -E -i '' "s_( href=)('|\")([^/])_\1\2/\3_g" {} \;
find $1 -name "*.html" -exec sed -E -i '' "s_( src=)('|\")([^/])_\1\2/\3_g" {} \;
find $1 -name "*.html" -exec sed -i '' "s_<html>_<html xmlns:th=\"\">_g" {} \;
